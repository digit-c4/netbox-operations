# Netbox

DIGIT C4 Netbox processes and Services tool kit

Services provided by this project:

* [Change Netbox Users Password](playbooks/change_users_password)
* [Init Development & Testing Vault Instance](playbooks/init_vault)
* [Dump & Restore Netbox data](playbooks/dump_and_restore)
* [Backup Netbox Dump file](playbooks/backup_and_retrieve)

`Netbox docker plugin` related services:

* [Library to create Containers using Netbox docker plugin](playbooks/library/netbox_docker_vm)
* [Role to Create or Upgrade Netbox using Netbox docker plugin](playbooks/roles/netbox_galaxy)

## How to contribute

### Requirements

Before starting development, be sure you are installed and functional:

* Python version up to 3.11
* Latest version of Poetry (see https://python-poetry.org/docs/#installing-manually)
* Docker

### Install your development environment

1. Clone this project
2. At the root of the project, install project dependencies:
   ```console
   poetry install
   ```
3. Start Netbox instance and others services:
   ```console
   env TAG=latest docker-compose up -d
   ```
4. Initialize local Vault instance:
   ```console
   env VAULT_ADDR=http://localhost:8200 VAULT_AUTH_METHOD=token VAULT_TOKEN=token ansible-playbook playbooks/init_vault/init_vault.yml --extra-vars "@playbooks/init_vault/vars/dev.json"
   ```

### Submitting your changes

1. Start your work on a new branch based on `main` branch. Be sure you have the
   latest version of the `main` branch.
2. When you work is finished, create a merge request.

## Execute tests

Be sure the test environment is installed:

1. At the root of the project, install project dependencies:
   ```console
   poetry install --only=test
   ```
2. Start Netbox instance and others services:
   ```console
   env TAG=latest docker-compose up -d
   ```
3. Initialize local Vault instance:
   ```console
   env VAULT_ADDR=http://localhost:8200 VAULT_AUTH_METHOD=token VAULT_TOKEN=token ansible-playbook playbooks/init_vault/init_vault.yml --extra-vars "@playbooks/init_vault/vars/dev.json"
   ```

With pytest at the root of the project:

```console
pytest tests/
```
