import pymongo
import requests
import json
import os
import datetime

# Connect to the MongoDB database with authentication
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')       
apitoken = os.getenv('API_TOKEN')
lab = os.getenv('NETBOX_IP')  
port = 80

client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
db = client['SID']
deadletter_collection = db['deadletter_rp']

base_url = f'http://{lab}:{port}/api/dcim/'
# URL of the endpoints to which POST requests will be made
devices_url = f'{base_url}devices/'
interfaces_url = f'{base_url}interfaces/'
device_type_url = f'{base_url}device-types/'
device_role_url = f'{base_url}device-roles/'
manufacturer_url = f'{base_url}manufacturers/'
rear_ports_url = f'{base_url}rear-ports/'
front_ports_url = f'{base_url}front-ports/'
# cables_url = f'{base_url}cables/'

# Headers for the POST request, including the CSRF token for authentication
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'pEdKVvFzQOugYoqCnFRC1JKUFFLukb6jBJyjrVN6vvBilzuAW3K4sPRYhCuQZO5S',
    'Authorization': f'Token {apitoken}'
}

# Send data to the endpoint and handle the response
def send_data(url, data):
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response

# Function to save failed posts to the deadletter collection
def save_to_deadletter(type, data, reason):
    deadletter_data = {
        "type": type,
        "data": data,
        "reason": reason,
        "timestamp": datetime.datetime.now()
    }
    deadletter_collection.insert_one(deadletter_data)

# Function to process deadletter entries
def process_deadletters():
    deadletters = list(deadletter_collection.find())
    for entry in deadletters:
        device_type = entry.get('type')
        data = entry.get('data')
        if device_type == 'patch_panel' or device_type == 'plug':
            url = devices_url
        elif device_type == 'rear_port':
            url = rear_ports_url
        elif device_type == 'front_port':
            url = front_ports_url
        else:         # Add more conditions for other device types as needed
            print(f"UNKNOWN DEVICE TYPE")
        
        response = send_data(url, data)
        if response.status_code == 200 or response.status_code == 201:
            deadletter_collection.delete_one({'_id': entry['_id']})
            print(f"Successfully reprocessed {device_type}: {data.get('name', 'N/A')}")
        else:
            save_to_deadletter(device_type, data, response.text)
            print(f"Failed to reprocess {device_type}: {data.get('name', 'N/A')} - {response.text}")

# Main script execution
if __name__ == "__main__":
    process_deadletters()