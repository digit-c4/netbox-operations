import pymongo
import os
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

username = os.getenv('MONGO_USERNAME')
password = os.getenv('MONGO_PASSWORD')

# Connect to MongoDB
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/?authSource=admin")
db = client['SID']
locations_collection = 'isp_plugs'

# SITES - BUILDINGS

# Define view pipeline
sites_pipeline = [

    {"$group": {
        "_id": {
            "building": {"$first": "$building.value"},
            "slug": {"$toLower": [{"$first": "$building.value"}]},
        },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.building",
        "slug": "$_id.slug",
        "group": {
            "name": "ISP"
        },
        "region": {
            "name": "Italy"
        },
        "status": "active"
        }
    }
]

# Create view
view_name = 'sites'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': locations_collection,
    'pipeline': sites_pipeline
})

print(f"View '{view_name}' created successfully.")

# WINGS

# Define view pipeline
wings_pipeline = [

    {"$group": {
        "_id": {
            "wing": { "$substr": [
                                {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '.', '$$this']}}}
                                , 1, -1]},
            "slug": {"$toLower": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                        "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}
                                ]},
            "site": {"$first": "$building.value"},
        },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.wing",
        "slug": "$_id.slug",
        "site": {
            "name": "$_id.site"
        },
        "status": "active",
        "description": "Wing"
        }
    }
]

# Create view
view_name = 'wings'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': locations_collection,
    'pipeline': wings_pipeline
})

print(f"View '{view_name}' created successfully.")

# FLOORS

# Define the first pipeline
pipeline1 = [
    {
        "$group": {
            "_id": {
                "name": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 3]},
                                                "initialValue": '',
                                                "in": {"$concat": ['$$value', '.', '$$this']}
                                                }
                                }, 1, -1]},
                "site": {"$first": "$building.value"},
                "slug": {"$toLower": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 3]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}
                                            }}
                ]},
                "parent": {"$substr": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '.', '$$this']}}}
                    , 1, -1]},
            }
    }},

    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": {"name": "$_id.site"},
            "status": "active",
            "parent": {"name": "$_id.parent"},
            "description": "Floor"
        }
    }
]

# Define the second pipeline
pipeline2 = [
    {
        "$group": {
            "_id": {
                "name": {"$first": "$floor.value"},
                "site": {"$first": "$building.value"},
                "slug": {"$toLower": [
                    {"$reduce": {"input": {"$slice": [{"$split": [{"$first": "$floor.value"}, "."]}, 3]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '$$this']}}}
                ]},
                "parent": {"$substr": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '.', '$$this']}}}
                    , 1, -1]},
            }
    }},

    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": {"name": "$_id.site"},
            "status": "active",
            "parent": {"name": "$_id.parent"},
            "description": "Floor"
        }
    }
]

# Combine the pipelines with unionWith
floors_pipeline = pipeline1 + [
    {
        "$unionWith": {
            "coll": "isp_plugs",
            "pipeline": pipeline2
        }
    },
    {
        "$group": {
            "_id": {"name": "$name", "site": "$site", "slug": "$slug", "parent": "$parent", "description": "$description"}
        }
    },
    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": "$_id.site",
            "status": "active",
            "parent": "$_id.parent",
            "description": "$_id.description"
        }
    }
]

# Create view
view_name = 'floors'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': locations_collection,
    'pipeline': floors_pipeline
})

print(f"View '{view_name}' created successfully.")

# ROOMS

# Define view pipeline
rooms_pipeline = [

    {
        "$group": {
        "_id": {
            "room": {"$concat": [{"$first": "$floor.value"}, '.', 'LR']} ,
            "site":  {"$first": "$building.value"},
            "slug": {"$toLower": [
                                {"$reduce": {"input": {"$slice": [{"$split": [{"$first": "$floor.value"}, "."]}, 3]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}
                                ]},
            "parent": {"$first": "$floor.value"},
        }}
    },

    {
        "$project": {"_id": 0,
                    "name": "$_id.room",
                    "slug": "$_id.slug",
                    "site": {
                        "name": "$_id.site"
                    },
                    "status": "active",
                    "parent": {
                        "name": "$_id.parent"
                    },
                    "description": "Room"
                  }
       }
]

# Create view
view_name = 'rooms'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': locations_collection,
    'pipeline': rooms_pipeline
})

print(f"View '{view_name}' created successfully.")