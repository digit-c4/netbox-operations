import datetime
import pymongo
import requests
import json
import os
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

# Connect to the MongoDB database with authentication
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')       
apitoken = os.getenv('API_TOKEN')
lab = os.getenv('NETBOX_IP')  
port = 80

client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
db = client['SID']
plug_devices_collection = db['plug_devices']
plug_rearport_pipeline = db['plug_rearport']
plug_frontport_pipeline = db['plug_frontport']
patch_panels_collection = db['patch_panels']
rear_ports_collection = db['rear_ports']
front_ports_collection = db['front_ports']
cables_collection = db['cables']
cables_collection2 = db['cables2']
locations_collection = db['locations']

deadletter_collection = db['deadletter']

base_url = f'http://{lab}:{port}/api/dcim/'
# URL of the endpoints to which POST requests will be made
devices_url = f'{base_url}devices/'
interfaces_url = f'{base_url}interfaces/'
device_type_url = f'{base_url}device-types/'
device_role_url = f'{base_url}device-roles/'
manufacturer_url = f'{base_url}manufacturers/'
rear_ports_url =f'{base_url}rear-ports/'
front_ports_url =f'{base_url}front-ports/'
# cables_url =f'{base_url}cables/'
locations_url = f'{base_url}locations/'


# Pagination configuration
batch_size = 1000

# Headers for the POST request, including the CSRF token for authentication
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'pEdKVvFzQOugYoqCnFRC1JKUFFLukb6jBJyjrVN6vvBilzuAW3K4sPRYhCuQZO5S',
    'Authorization': f'Token {apitoken}'    
}

# Send data to the endpoint and handle the response
def send_data(url, data):
    response = requests.post(url, headers=headers, data=json.dumps(data))
    # if response.status_code != 200 and response.status_code != 201:
    #     print(f"Failed to send data: {response.status_code}, {response.text}")
    #     return response
    # print(f"Response: {response.status_code}, {response.text}")
    return response

# Function to save failed posts to the deadletter collection
def save_to_deadletter(type, data, reason):
    deadletter_data = {
        "type": type,
        "data": data,
        "reason": reason,
        "timestamp": datetime.datetime.now()
    }
    deadletter_collection.insert_one(deadletter_data)

# PLUG DEVICES

# Step 1: Create role
plug_device_role_data = {
    "name": "User plug",
    "slug": "User_plug",
}

plug_device_role_response = send_data(device_role_url, plug_device_role_data)
plug_device_role_id = plug_device_role_response.json().get('id')

if not plug_device_role_id:
    save_to_deadletter(plug_device_role_data, plug_device_role_response.text)
    print(f"Failed to create device role {plug_device_role_data['name']} - {plug_device_role_response.text}")
else: print("User plug role created")

# Step 2: Create the manufacturer
manufacturer_data = {
    "name": "PP",
    "slug": "PP",
    "description": "Patch panel"
}

manufacturer_response  = send_data(manufacturer_url, manufacturer_data)
manufacturer_id  = manufacturer_response.json().get('id')

if not manufacturer_id :
    save_to_deadletter(manufacturer_data, manufacturer_response.text)
    print(f"Failed to create manufacturer {manufacturer_data['name']} - {manufacturer_response.text}")
else: print(f"Manufacturer {manufacturer_data['name']} created")

# Step 3: Create the device type
plug_device_type_data = {
    "model": "Network_plugs",
    "slug": "Network_plugs",
    "manufacturer": manufacturer_id
}

plug_device_type_response = send_data(device_type_url, plug_device_type_data)
plug_device_type_id = plug_device_type_response.json().get('id')

if not plug_device_type_id:
    save_to_deadletter(plug_device_type_data, plug_device_type_response.text)
    print(f"Failed to create device type {plug_device_type_data['model']} - {plug_device_type_response.text}")
else: print("Network_plugs device type created")

skip = 0
while True:
    device_data = list(plug_devices_collection.find().skip(skip).limit(batch_size))
    if not device_data:
        break
    plug_devices_response = send_data(devices_url, device_data)
    if plug_devices_response.status_code==400: 
        save_to_deadletter(device_data, plug_devices_response.text)
        print(f"Failed PLUG DEVICES:{device_data[0]['name']} and {batch_size} more - {plug_devices_response.text}")
    else: print(f"PLUG DEVICES:{device_data[0]['name']} and following {batch_size} created")
    skip += batch_size

# PLUG REARPORT
skip = 0
while True:
    plug_rearport_data = list(plug_rearport_pipeline.find().skip(skip).limit(batch_size))
    if not plug_rearport_data:
        break
    plug_interfaces_response = send_data(rear_ports_url, plug_rearport_data)
    if plug_interfaces_response.status_code==400: 
        save_to_deadletter('plug_rearport', plug_rearport_data, plug_interfaces_response.text)
        print(f"Failed PLUG REARPROT: {plug_rearport_data[0]['name']} and {batch_size} following - {plug_interfaces_response.text}")
    else: print(f"PLUG REARPROT:{plug_rearport_data[0]['name']} and following {batch_size} created")
    skip += batch_size


# PLUG FRONTPORT
skip = 0
while True:
    plug_frontport_data = list(plug_frontport_pipeline.find().skip(skip).limit(batch_size))
    if not plug_frontport_data:
        break
    plug_interfaces_response = send_data(front_ports_url, plug_frontport_data)
    if plug_interfaces_response.status_code==400: 
        save_to_deadletter('plug_frontport', plug_frontport_data, plug_interfaces_response.text)
        print(f"Failed PLUG REARPROT: {plug_frontport_data[0]['name']} and {batch_size} following - {plug_interfaces_response.text}")
    else: print(f"PLUG REARPROT:{plug_frontport_data[0]['name']} and following {batch_size} created")
    skip += batch_size

# PATCH PANEL DEVICES

# Step 1: Create the device role
pp_device_role_data = {
    "name": "Patch Panel RJ45",
    "slug": "patch-panel-rj45",
}

pp_device_role_response = send_data(device_role_url, pp_device_role_data)
pp_device_role_id = pp_device_role_response.json().get('id')

if not pp_device_role_id:
    save_to_deadletter(pp_device_role_data, pp_device_role_response.text)
    print(f"Failed to create device role {pp_device_role_data['name']} - {pp_device_role_response.text}")
else: print("Patch Panel RJ45 role created")

# Step 2: Create the device type
pp_device_type_data = {
    "model": "PP_RJ45",
    "slug": "pp_rj45",
    "manufacturer": manufacturer_id
}

pp_device_type_response = send_data(device_type_url, pp_device_type_data)
pp_device_type_id = pp_device_type_response.json().get('id')

if not pp_device_type_id:
    save_to_deadletter(pp_device_type_data, pp_device_type_response.text)
    print(f"Failed to create device type {pp_device_type_data['model']} - {pp_device_type_response.text}")
else: print("PP_RJ45 device type created")


# Step 3: Create patch panel devices

patch_panels_data = list(patch_panels_collection.find())
# Paginate the data in memory
for i in range(0, len(patch_panels_data), batch_size):
    batch = patch_panels_data[i:i + batch_size]
    for patch_panel in batch:
        patch_panels_response = send_data(devices_url, patch_panel)
        if patch_panels_response.status_code != 200 and patch_panels_response.status_code != 201:
            save_to_deadletter('patch_panel', patch_panel, patch_panels_response.text)
            print(f"Failed PATCH PANELS: {patch_panel['name']} - {patch_panels_response.text}")
        else:
            print(f"PATCH PANELS created: {patch_panel['name']} - {patch_panels_response.text}")

# REAR PORTS

rear_ports_data = list(rear_ports_collection.find())
# Paginate the data in memory
for i in range(0, len(rear_ports_data), batch_size):
    batch = rear_ports_data[i:i + batch_size]
    for rear_port in batch:
        rear_ports_response = send_data(rear_ports_url, rear_port)
        if rear_ports_response.status_code != 200 and rear_ports_response.status_code != 201:
            save_to_deadletter('rear_port', rear_port, rear_ports_response.text)
            print(f"Failed REAR PORTL: {rear_port['name']} - {rear_ports_response.text}")
        else:
            print(f"REAR PORT created: {rear_port['name']} - {rear_ports_response.text}")


# FRONT PORTS

front_ports_data = list(front_ports_collection.find())
# Paginate the data in memory
for i in range(0, len(front_ports_data), batch_size):
    batch = front_ports_data[i:i + batch_size]
    for front_port in batch:
        front_ports_response = send_data(front_ports_url, front_port)
        if front_ports_response.status_code != 200 and front_ports_response.status_code != 201:
            save_to_deadletter('front_port', front_port, front_ports_response.text)
            print(f"Failed FRONT PORTL: {front_port['name']} - {front_ports_response.text}")
        else:
            print(f"FRONT PORT created: {front_port['name']} - {front_ports_response.text}")



 # CABLES
cables_data = list(cables_collection.find())
print("******** PREPARING CABLES BULK ********")

# File writing
with open('cables_bulk.json', 'w') as file:
    file.seek(0)
    cables_data = json.dumps(cables_data, cls=json.JSONEncoder, indent=4)
    file.write(cables_data)
    file.truncate()

print("Data written to cables_bulk.json successfully.")  


 # CABLES 2 - Network_plug -> PP
cables_data = list(cables_collection2.find())
print("******** PREPARING CABLES BULK ********")

# File writing
with open('cables_bulk2 .json', 'w') as file:
    file.seek(0)
    cables_data = json.dumps(cables_data, cls=json.JSONEncoder, indent=4)
    file.write(cables_data)
    file.truncate()

print("Data written to cables_bulk.json successfully.")  


# # # LOCATIONS

# missing_locations_collection = db['missing_locations']
# locations_data = list(missing_locations_collection.find())
# # Paginate the data in memory
# for i in range(0, len(locations_data), batch_size):
#     batch = locations_data[i:i + batch_size]
#     for location in batch:
#         locations_response = send_data(locations_url, location)
#         if locations_response.status_code != 200 and locations_response.status_code != 201:
#             print(f"Failed LOCATION: {location['name']} - {locations_response.text}")
#             save_to_deadletter('location', location, locations_response.text)
#         else:
#             print(f"LOCATION created: {location['name']} - {locations_response.text}")