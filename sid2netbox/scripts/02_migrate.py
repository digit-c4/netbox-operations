import argparse
import datetime
import pymongo
import requests
import json
import os
import time
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

# Load env variables from .env file

username = os.getenv('MONGO_USERNAME')
password = os.getenv('MONGO_PASSWORD')
apitoken = os.getenv('API_TOKEN')
lab = os.getenv('NETBOX_IP')  
port = os.getenv('PORT') 

# Connect to MongoDB
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/?authSource=admin")
db = client['SID']

deadletter_collection = db['deadletter']
sites_collection = db['sites']
plug_devices_collection = db['plug_devices']
patch_panels_collection = db['patch_panels']
plug_rearport_collection = db['plug_rearport']
plug_frontport_collection = db['plug_frontport']
pp_rearport_collection = db['pp_rear_ports']
pp_frontport_collection = db['pp_front_ports']
cables_network_device2pp_collection = db['cables_network_device2pp']
cables_pp2network_plug_collection = db['cables_pp2network_plug']

# URL of the endpoints to which POST requests will be made
base_url = f'http://{lab}:{port}/api/dcim/'

devices_url = f'{base_url}devices/'
device_type_url = f'{base_url}device-types/'
device_role_url = f'{base_url}device-roles/'
manufacturer_url = f'{base_url}manufacturers/'
rear_ports_url =f'{base_url}rear-ports/'
front_ports_url =f'{base_url}front-ports/'
regions_url =f'{base_url}regions/'
site_groups_url =f'{base_url}site-groups/'
locations_url = f'{base_url}locations/'
sites_url = f'{base_url}sites/'

# Headers for the POST request, including the CSRF token for authentication
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'pEdKVvFzQOugYoqCnFRC1JKUFFLukb6jBJyjrVN6vvBilzuAW3K4sPRYhCuQZO5S',
    'Authorization': f'Token {apitoken}'    
}

# Send data to the endpoint and handle the response
def send_data(url, data):
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response

# Function to save failed posts to the deadletter collection
def save_to_deadletter(type, data, reason):
    deadletter_data = {
        "type": type,
        "data": data,
        "reason": reason,
        "timestamp": datetime.datetime.now()
    }
    deadletter_collection.insert_one(deadletter_data)

# Helper function to split data into batches
def batch_data(data, batch_size):
    for i in range(0, len(data), batch_size):
        yield data[i:i + batch_size]

# Function to prepare roles, manufacturer, device types, region adn site group
def prepare_organisation(batch_size=1):

    print(f"Preparing environment...")
        # Step 1: Create user plug role
    plug_device_role_data = {
        "name": "User plug",
        "slug": "user-plug",
    }

    plug_device_role_response = send_data(device_role_url, plug_device_role_data)
    plug_device_role_id = plug_device_role_response.json().get('id')

    if not plug_device_role_id:
        save_to_deadletter('plug_device_role', plug_device_role_data, plug_device_role_response.text)
        print(f"Failed to create device role {plug_device_role_data['name']} - {plug_device_role_response.text}")
    else: print("User plug role created")

    # Step 2: Create PP manufacturer
    manufacturer_data = {
        "name": "PP",
        "slug": "PP",
        "description": "Patch panel"
    }

    manufacturer_response  = send_data(manufacturer_url, manufacturer_data)
    manufacturer_id  = manufacturer_response.json().get('id')

    if not manufacturer_id :
        save_to_deadletter('manufacturer', manufacturer_data, manufacturer_response.text)
        print(f"Failed to create manufacturer {manufacturer_data['name']} - {manufacturer_response.text}")
    else: 
        print(f"Manufacturer {manufacturer_data['name']} created")

    # Step 3: Create plug device type
    if manufacturer_id:
        plug_device_type_data = {
            "model": "Network_plugs",
            "slug": "network_plugs",
            "manufacturer": manufacturer_id
        }

        plug_device_type_response = send_data(device_type_url, plug_device_type_data)
        plug_device_type_id = plug_device_type_response.json().get('id')

        if not plug_device_type_id:
            save_to_deadletter('plug_device_type', plug_device_type_data, plug_device_type_response.text)
            print(f"Failed to create device type {plug_device_type_data['model']} - {plug_device_type_response.text}")
        else: print("Network_plugs device type created")

    # Step 4: Create PP role
    pp_device_role_data = {
        "name": "Patch Panel RJ45",
        "slug": "patch-panel-rj45",
    }

    pp_device_role_response = send_data(device_role_url, pp_device_role_data)
    pp_device_role_id = pp_device_role_response.json().get('id')

    if not pp_device_role_id:
        save_to_deadletter('pp_device_role', pp_device_role_data, pp_device_role_response.text)
        print(f"Failed to create device role {pp_device_role_data['name']} - {pp_device_role_response.text}")
    else: print("Patch Panel RJ45 role created")

    # Step 5: Create PP device type
    if manufacturer_id:
        pp_device_type_data = {
            "model": "PP_RJ45",
            "slug": "pp_rj45",
            "manufacturer": manufacturer_id
        }

        pp_device_type_response = send_data(device_type_url, pp_device_type_data)
        pp_device_type_id = pp_device_type_response.json().get('id')

        if not pp_device_type_id:
            save_to_deadletter('pp_device_type', pp_device_type_data, pp_device_type_response.text)
            print(f"Failed to create device type {pp_device_type_data['model']} - {pp_device_type_response.text}")
        else: print("PP_RJ45 device type created")

    # Step 6: Create region
    region_data = {
        "name": "Italy",
        "description": "Ispra",
        "slug": "italy"
    }

    region_response = send_data(regions_url, region_data)
    region_id = region_response.json().get('id')

    if not region_id:
        save_to_deadletter('region', region_data, region_response.text)
        print(f"Failed to create region {region_data['name']} - {region_response.text}")
    else: print("Region created")

    # Step 7: Create site groups
    site_group_data = {
        "name": "ISP",
        "slug": "isp"
    }

    site_group_response = send_data(site_groups_url, site_group_data)
    site_group_id = site_group_response.json().get('id')

    if not site_group_id:
        save_to_deadletter('site_group', site_group_data, site_group_response.text)
        print(f"Failed to create site group {site_group_data['name']} - {site_group_response.text}")
    else: print("Site group created")

# Function to handle sites operation
def handle_site(batch_size=1):
    start_time = time.time()  # Start timing the operation
    # Retrieve data
    site_data = list(sites_collection.find())

    # Send POST requests
    sites_created = 0
    print("******** CREATING SITES ********")

    for batch in batch_data(site_data, batch_size):
        response = send_data(sites_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('site', batch, response.text)
            print(f"Failed SITE batch: {response.text}")
        else:
            sites_created += len(batch)
    
    duration = time.time() - start_time
    print(f"{sites_created} SITES created in {duration:.2f} seconds")

# Function to handle locations operation
def handle_location(batch_size=1, location_view=None):
    if location_view is None:
        raise ValueError("The --location-view argument is required when running the location operation.")
    start_time = time.time()  # Start timing the operation

    locations_collection = db[location_view]
    # Retrieve data
    location_data = list(locations_collection.find())

    # Send POST requests
    locations_created = 0
    print(f"******** CREATING LOCATIONS: {location_view} ********")

    for batch in batch_data(location_data, batch_size):
        response = send_data(locations_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('location', batch, response.text)
            print(f"Failed LOCATION batch: {response.text}")
        else:
            locations_created += len(batch)
    
    duration = time.time() - start_time
    print(f"{locations_created} LOCATIONS from {location_view} created in {duration:.2f} seconds")

# Function to handle plug devices operation
def handle_plug_device(batch_size=1):
    start_time = time.time()  # Start timing the operation
    # Retrieve data
    device_data = list(plug_devices_collection.find())

    # Send POST requests
    devices_created = 0
    print("******** CREATING PLUG DEVICES ********")

    for batch in batch_data(device_data, batch_size):
        response = send_data(devices_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('plug_device', batch, response.text)
            print(f"Failed PLUG DEVICE batch: {response.text}")
        else:
            devices_created += len(batch)
    
    duration = time.time() - start_time
    print(f"{devices_created} PLUG DEVICES created in {duration:.2f} seconds")

# Function to handle plug rearport operation
def handle_plug_rearport(batch_size=1):
    start_time = time.time()  # Start timing the operation
    plug_rearport_data = list(plug_rearport_collection.find())

    # Send POST requests
    plug_rearports_created = 0
    print("******** CREATING PLUG REAR PORTS ********")
    for batch in batch_data(plug_rearport_data, batch_size):
        response = send_data(rear_ports_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('plug_rearport', batch, response.text)
            print(f"Failed PLUG REAR PORT batch: {response.text}")
        else:
            plug_rearports_created += len(batch)

    duration = time.time() - start_time
    print(f"{plug_rearports_created} PLUG REAR PORTS created in {duration:.2f} seconds")

# Function to handle plug frontport operation
def handle_plug_frontport(batch_size=1):
    start_time = time.time()  # Start timing the operation
    plug_frontport_data = list(plug_frontport_collection.find())

    # Send POST requests
    plug_frontports_created = 0
    print("******** CREATING PLUG FRONT PORTS ********")
    for batch in batch_data(plug_frontport_data, batch_size):
        response = send_data(front_ports_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('plug_frontport', batch, response.text)
            print(f"Failed PLUG FRONT PORT batch: {response.text}")
        else:
            plug_frontports_created += len(batch)

    duration = time.time() - start_time
    print(f"{plug_frontports_created} PLUG FRONT PORTS created in {duration:.2f} seconds")

# Function to handle patch panels operation
def handle_patch_panel(batch_size=1):
    start_time = time.time()  # Start timing the operation
    patch_panels_data = list(patch_panels_collection.find())
    
    # Send POST requests
    patch_panels_created = 0
    print("******** CREATING PATCH PANELS ********")
    for batch in batch_data(patch_panels_data, batch_size):
        response = send_data(devices_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('patch_panel_device', batch, response.text)
            print(f"Failed PATCH PANEL batch: {response.text}")
        else:
            patch_panels_created += len(batch)

    duration = time.time() - start_time
    print(f"{patch_panels_created} PATCH PANEL DEVICES created in {duration:.2f} seconds")

# Function to handle patch_panels rearport operation operation
def handle_pp_rearport(batch_size=1):
    start_time = time.time()  # Start timing the operation
    pp_rearport_data = list(pp_rearport_collection.find())

    # Send POST requests
    pp_rearports_created = 0
    print("******** CREATING PP REAR PORTS ********")
    for batch in batch_data(pp_rearport_data, batch_size):
        response = send_data(rear_ports_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('pp_rearport', batch, response.text)
            print(f"Failed PP REAR PORT batch: {response.text}")
        else:
            pp_rearports_created += len(batch)

    duration = time.time() - start_time
    print(f"{pp_rearports_created} PP REAR PORTS created in {duration:.2f} seconds")

# Function to handle patch_panels frontport operation operation
def handle_pp_frontport(batch_size=1):
    start_time = time.time()  # Start timing the operation
    pp_frontport_data = list(pp_frontport_collection.find())

    # Send POST requests
    pp_frontports_created = 0
    print("******** CREATING PP FRONT PORTS ********")
    for batch in batch_data(pp_frontport_data, batch_size):
        response = send_data(front_ports_url, batch)
        if response.status_code not in {200, 201}:
            save_to_deadletter('pp_frontport', batch, response.text)
            print(f"Failed PP FRONT PORT batch: {response.text}")
        else:
            pp_frontports_created += len(batch)

    duration = time.time() - start_time
    print(f"{pp_frontports_created} PP FRONT PORTS created in {duration:.2f} seconds")

# Function to handle cable_network_device2pp operation
def handle_cable_network_device2pp(batch_size=1):
    start_time = time.time()  # Start timing the operation
    cables_network_device2pp_data = list(cables_network_device2pp_collection.find())
    print("******** PREPARING CABLES NETWORK DEVICE TO PP BULK ********")

    output_path='sid2netbox/data/cables/'

    if not os.path.isdir(output_path):
        os.mkdir(output_path)

    # File writing
    with open(output_path + 'cables_network_device2pp_bulk.json', 'w') as file:
        file.seek(0)
        cables_network_device2pp_data = json.dumps(cables_network_device2pp_data, cls=json.JSONEncoder, indent=4)
        file.write(cables_network_device2pp_data)
        file.truncate()

    duration = time.time() - start_time
    print(f"Data written to cables_network_device2pp_bulk.json successfully in {duration:.2f} seconds.")  

# Function to handle cable_pp2network_plug operation
def handle_cable_pp2network_plug(batch_size=1):
    start_time = time.time()  # Start timing the operation
    cables_pp2network_plug_data = list(cables_pp2network_plug_collection.find())
    print("******** PREPARING CABLES NETWORK DEVICE TO PP BULK ********")

    output_path='sid2netbox/data/cables/'

    if not os.path.isdir(output_path):
        os.mkdir(output_path)
    # File writing
    with open(output_path + 'cables_pp2network_plug_bulk.json', 'w') as file:
        file.seek(0)
        cables_pp2network_plug_data = json.dumps(cables_pp2network_plug_data, cls=json.JSONEncoder, indent=4)
        file.write(cables_pp2network_plug_data)
        file.truncate()

    duration = time.time() - start_time
    print(f"Data written to cables_pp2network_plug_bulk.json successfully in {duration:.2f} seconds.")  

# Mapping operation names to corresponding functions
operation_functions = {
    'organisation': prepare_organisation,
    'site': handle_site,
    'location': handle_location,
    'plug_device': handle_plug_device,
    'plug_rearport': handle_plug_rearport,
    'plug_frontport': handle_plug_frontport,
    'patch_panel': handle_patch_panel,
    'pp_rearport': handle_pp_rearport,
    'pp_frontport': handle_pp_frontport,
    'cable_network_device2pp': handle_cable_network_device2pp,
    'cable_pp2network_plug': handle_cable_pp2network_plug
}


# Main function to parse arguments and call respective functions
def main():
    parser = argparse.ArgumentParser(description="Manage script for various operations.")
    parser.add_argument('operations', nargs='*', 
                        help="The operations to perform in order.",
                        choices=['all', 'organisation', 'site', 'location', 'plug_device', 'plug_rearport', 'plug_frontport', 'patch_panel', 
                                 'pp_rearport', 'pp_frontport', 'cable_network_device2pp', 'cable_pp2network_plug'])
    parser.add_argument('--batch-size', type=int, default=1, help="Number of items per batch.")
    parser.add_argument('--location-views',  nargs='*', type=str, help="Specify one or more location views to migrate")
    # buildings - wings - floors - rooms
    parser.add_argument('--skip-location', action='store_true', help="Skip the location operation even if 'all' is specified.")

    args = parser.parse_args()

    # Determine operations to run
    if 'all' in args.operations:
        operations_to_run = list(operation_functions.keys())
        if args.skip_location:
            operations_to_run.remove('location')
    else:
        operations_to_run = args.operations

    # Start timing the entire process
    total_start_time = time.time()

    for operation in operations_to_run:
        if operation in operation_functions:
            if operation == 'location':
                if not args.location_views:
                    raise ValueError("The --location-views argument is required for the 'location' operation.")
                for view in args.location_views:
                    operation_functions[operation](batch_size=args.batch_size, location_view=view)
            else:
                operation_functions[operation](batch_size=args.batch_size)
        else:
            print(f"Unknown operation: {operation}")
    
    # Calculate the total duration
    total_duration = time.time() - total_start_time
    print(f"Total duration for all operations: {total_duration:.2f} seconds")


if __name__ == "__main__":
    main()