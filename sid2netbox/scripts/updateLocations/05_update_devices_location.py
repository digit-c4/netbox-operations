import pymongo
import os
import json
import requests
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

# MongoDB connection details
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')  
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
apitoken = os.getenv('API_TOKEN')
db = client['SID']
lab = os.getenv('NETBOX_IP')  
port = os.getenv('PORT') 

# Collection
devices_migration_updated_collection = db['devices_updated_location']
devices_migration_errors_collection = db['devices_update_errors']

# NetBox API details
base_url = f'http://{lab}:{port}/api/dcim/'
devices_url = f'{base_url}devices/'

headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'PIjJ6kQ7XgQQj47fjwVhgqkRD0voTaPBDJtgEJzapmARGJUGdoi6i2AkKOQXwxyE',
    'Authorization': f'Token {apitoken}' 
}

updated_devices = 0

# Function to send PUT request
def update_device(device):
    global updated_devices
    response = requests.put(devices_url, headers=headers, data=json.dumps([device]))
    if response.status_code == 200:
        updated_devices += 1
        # print(f"Successfully updated device with id {device['id']}")
    else:
        print(f"Failed to update device with id {device['id']}: {response.status_code} - {response.text}")
        error_put = {
        "id": device['id'],
        "device_type": device['device_type'],
        "role": device['role'],
        "site": device['site'],
        "location": device['location'],
        "error": response.text
        }
        devices_migration_errors_collection.insert_one(error_put)
        print(f"Inserted device with id {device['id']} into deadletter collection.")

# Fetch devices from the devices_migration_updated collection
devices = list(devices_migration_updated_collection.find())

# Update each device via the NetBox API
for device in devices:
    update_device({
        "id": device['id'],
        "device_type": device['device_type'],
        "role": device['role'],
        "site": device['site'],
        "location": device['location']
    })

print(f"Successfully updated {updated_devices} devices")