import pymongo
import requests
import os
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 


# Connect to the MongoDB database with authentication
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')       
apitoken = os.getenv('API_TOKEN')
lab = os.getenv('NETBOX_IP') 
port = os.getenv('PORT') 

client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
db = client['SID']
devices_migration_collection = db['devices_migration']

# NetBox API details
base_url = f'http://{lab}:{port}/api/dcim/'


# Headers for the POST request, including the CSRF token for authentication
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'YbkHIzTdCgUbxPXB42xZcMqDhYvJNxTd',
    'Authorization': f'Token {apitoken}'  
}

# Function to get devices from NetBox API with filtering
def get_filtered_devices():
    devices = []
    devices_url = f"{base_url}devices/"
    params = {
        'site': 'ispra',
        'limit': 100,  # limit for pagination
    }
    #excluded_roles_params = 'role__n=patch-panel-rj45&role__n=user-plug'

    excluded_roles = ['patch-panel-rj45','user-plug','access-point','SYS']
    
    excluded_roles_params = '&'.join([f'role__n={role}' for role in excluded_roles])
   # included_roles_params = '&'.join([f'role={role}' for role in ['patch-panel-rj45']])


    while True:
        
        full_url = f"{devices_url}?{excluded_roles_params}&{requests.compat.urlencode(params)}"
        print("*** API CALL: ", full_url)

        response = requests.get(full_url, headers=headers)
        if response.status_code != 200:
            print(f"Failed to get data: {response.status_code}, {response.text}")
            break
        
        data = response.json()
        results = data.get('results', [])
        devices.extend(results)

        print("*** Retrieved devices: ", len(devices))

        if data.get('next'):
            params['offset'] = params.get('offset', 0) + params['limit']
        else:
            break
    return devices

# Step 1: Fetch and filter devices
filtered_devices = get_filtered_devices()

# Step 2: Save filtered devices to MongoDB
if filtered_devices:
    devices_migration_collection.insert_many(filtered_devices)
    print(f"Inserted {len(filtered_devices)} devices into the devices_migration collection.")
else:
    print("No devices to insert.")