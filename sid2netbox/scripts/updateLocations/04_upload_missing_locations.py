import datetime
import pymongo
import requests
import json
import os
import time
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv()  # Will search for .env file in local folder and load variables

# Connect to the MongoDB database with authentication
username = os.getenv('MONGO_USERNAME')
password = os.getenv('MONGO_PASSWORD')
apitoken = os.getenv('API_TOKEN')
lab = os.getenv('NETBOX_IP')
port = os.getenv('PORT')

client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/?authSource=admin")
db = client['SID']

existing_devices_wings_collection = db['existing_devices_wings']
wings_collection = db['wings']
existing_devices_floors_collection = db['existing_devices_floors']
floors_collection = db['floors']
existing_devices_sites_collection = db['existing_devices_sites']
sites_collection = db['sites']
existing_devices_rooms_collection = db['existing_devices_rooms']
rooms_collection = db['rooms']
deadletter_collection = db['deadletter']

base_url = f'http://{lab}:{port}/api/dcim/'
locations_url = f'{base_url}locations/'
sites_url = f'{base_url}sites/'

# Headers for the POST request, including the CSRF token for authentication
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'pEdKVvFzQOugYoqCnFRC1JKUFFLukb6jBJyjrVN6vvBilzuAW3K4sPRYhCuQZO5S',
    'Authorization': f'Token {apitoken}'
}

def send_data(url, data):
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response

def save_to_deadletter(type, data, reason):
    deadletter_data = {
        "type": type,
        "data": data,
        "reason": reason,
        "timestamp": datetime.datetime.now()
    }
    deadletter_collection.insert_one(deadletter_data)

def get_existing_names(collection, id_field):
    return {item[id_field] for item in collection.find({}, {id_field: 1})}

start_time = time.time()  # Start timing the operation

# Fetch all existing room, wing and floor names to minimize DB queries
existing_wing_ids = get_existing_names(wings_collection, 'name')
existing_floor_ids = get_existing_names(floors_collection, 'name')
existing_site_ids = get_existing_names(sites_collection, 'name')
existing_room_ids = get_existing_names(rooms_collection, 'name')



sites_created = 0
sites_data = list(existing_devices_sites_collection.find())

print(f"******** CREATING SITES ********")

# Process sites
for site in sites_data:
    site_id = site.get('name')
    if site_id and site_id not in existing_site_ids:
        response = send_data(sites_url, site)
        if response.status_code not in {200, 201}:
            save_to_deadletter('site', site, response.text)
            print(f"Failed SITE: {response.text}")
        else:
            sites_created += 1


wings_created = 0
wings_data = existing_devices_wings_collection.find()

print(f"******** CREATING WINGS, FLOORS AND ROOMS ********")

# Process wings
for wing in wings_data:
    wing_id = wing.get('name')
    if wing_id and wing_id not in existing_wing_ids:
        response = send_data(locations_url, wing)
        if response.status_code not in {200, 201}:
            save_to_deadletter('location', wing, response.text)
            print(f"Failed WING: {response.text}")
        else:
            wings_created += 1

# Process floors
floors_created = 0
floors_data = existing_devices_floors_collection.find()

for floor in floors_data:
    floor_id = floor.get('name')
    if floor_id and floor_id not in existing_floor_ids:
        response = send_data(locations_url, floor)
        if response.status_code not in {200, 201}:
            save_to_deadletter('floor', floor, response.text)
            print(f"Failed FLOOR: {response.text}")
        else:
            floors_created += 1

# Process rooms
rooms_created = 0
rooms_data = existing_devices_rooms_collection.find()

for room in rooms_data:
    room_id = room.get('name')
    if room_id and room_id not in existing_room_ids:
        response = send_data(locations_url, room)
        if response.status_code not in {200, 201}:
            save_to_deadletter('room', room, response.text)
            print(f"Failed ROOMS: {response.text}")
        else:
            rooms_created += 1

duration = time.time() - start_time
print(f"{sites_created} SITES, {wings_created} WINGS, {floors_created} FLOORS and {rooms_created} ROOMS created in {duration:.2f} seconds")
