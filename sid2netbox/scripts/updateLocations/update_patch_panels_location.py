import pymongo
import os
import json
import requests
import re

# MongoDB connection details
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')  
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
apitoken = os.getenv('API_TOKEN')
db = client['SID']

lab = os.getenv('NETBOX_IP') 
port = 80
base_url = f'http://{lab}:{port}/api/dcim/'


# Collection
devices_migration_errors_collection = db['patch_panels_update_errors']


# NetBox API details
url = 'http://cmdb.test.netbox.snmc.cec.eu.int/api/dcim/devices/'
headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json',
    'X-CSRFTOKEN': 'PIjJ6kQ7XgQQj47fjwVhgqkRD0voTaPBDJtgEJzapmARGJUGdoi6i2AkKOQXwxyE',
    'Authorization': f'Token {apitoken}' 
}

# Function to send PUT request
def update_device(device):
    response = requests.put(url, headers=headers, data=json.dumps([device]))
    if response.status_code == 200:
        print(f"Successfully updated device with id {device['id']}")
    else:
        print(f"Failed to update device with id {device['id']}: {response.status_code} - {response.text}")
        error_put = {
        "id": device['id'],
        "name": device['name'],
        "device_type": device['device_type'],
        "role": device['role'],
        "site": device['site'],
        "location": device['location'],
        "error": response.text
        }
        devices_migration_errors_collection.insert_one(error_put)
        print(f"Inserted device with id {device['id']} into deadletter collection.")

def get_filtered_devices():
    devices = []
    devices_url = f"{base_url}devices/"
    params = {
        'limit': 100,  # limit for pagination
    }

    included_roles_params = '&'.join([f'role={role}' for role in ['patch-panel-rj45']])

    while True:
        
        full_url = f"{devices_url}?{included_roles_params}&{requests.compat.urlencode(params)}"
        print("*** API CALL: ", full_url)

        response = requests.get(full_url, headers=headers)
        if response.status_code != 200:
            print(f"Failed to get data: {response.status_code}, {response.text}")
            break
        
        data = response.json()
        results = data.get('results', [])
        devices.extend(results)

        print("*** Retrieved patch panels: ", len(devices))

        if data.get('next'):
            params['offset'] = params.get('offset', 0) + params['limit']
        else:
            break
    return devices

# Function to transform device data
def transform_device(device):
    name = device['name']

    first_match = re.match(r'(.{4})\.(.*)', name)
    if first_match:
        site_name = first_match.group(1)

        transformed_device = {
            "id": device['id'],
            "name": device['name'],
            "device_type": device['device_type']['id'],
            "role": device['device_role']['id'],
            "site": {
                "name": site_name
            },
            "location": {
                "name": device['name']
            }
        }
        return transformed_device, None
    
    # If pattern does not match, return as error device
    error_device = {
        "id": device['id'],
        "name": device['name'],
        "device_type": device['device_type']['id'],
        "role": device['device_role']['id'],
        "error": "Unmatched pattern"
    }
    return None, error_device

# Step 1: Fetch and filter devices
patch_panels = get_filtered_devices()

# Step 2: Transform devices and separate valid and error devices
updated_devices = []
error_devices = []

for device in patch_panels:
    transformed_device, error_device = transform_device(device)
    if transformed_device:
        updated_devices.append(transformed_device)
    if error_device:
        error_devices.append(error_device)

# Insert error devices into the error collection
if error_devices:
    devices_migration_errors_collection.insert_many(error_devices)
    print(f"Inserted {len(error_devices)} error devices into the devices_migration_errors collection.")
else:
    print("No error devices to insert.")

# Step 3: Update each device via the NetBox API
for updated in updated_devices:
    update_device({
        "id": updated['id'],
        "device_type": updated['device_type'],
        "role": updated['role'],
        "site": updated['site'],
        "location": updated['location']
    })
