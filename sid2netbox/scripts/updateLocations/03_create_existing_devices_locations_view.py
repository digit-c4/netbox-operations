import pymongo
import os
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

# Connect to the MongoDB database with authentication
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')       

client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
db = client['SID']

existing_devices_locations_collection = 'devices_updated_location'

# SITES

# Define view pipeline
sites_pipeline = [
    {"$group": {
        "_id": {
            "building": "$site.name",
            "slug": {"$toLower": ["$site.name"]}
        },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.building",
        "slug": "$_id.slug",
        "group": {
            "name": "ISP"
        },
        "region": {
            "name": "Italy"
        },
        "status": "active"
        }
    }
]

# Create view
view_name = 'existing_devices_sites'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': existing_devices_locations_collection,
    'pipeline': sites_pipeline
})

print(f"View '{view_name}' created successfully.")

# WINGS

# Define view pipeline
wings_pipeline = [
    {"$group": {
        "_id": {
            "wing": { "$substr": [
                                {"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 2]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '.', '$$this']}}}
                                , 1, -1]},
            "slug": {"$toLower": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 2]},
                                        "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}
                                ]},
            "site": "$site"
            },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.wing",
        "slug": "$_id.slug",
        "site": "$_id.site",
        "status": "active",
        "description": "Wing"
        }
    }
]

# Create view
view_name = 'existing_devices_wings'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': existing_devices_locations_collection,
    'pipeline': wings_pipeline
})

print(f"View '{view_name}' created successfully.")

# FLOORS

# Define view pipeline
floors_pipeline = [
    {"$group": {
        "_id": {
            "floor": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 3]},
                                                "initialValue": '',
                                                "in": {"$concat": ['$$value', '.', '$$this']}
                                                }
                                }, 1, -1]},
            "slug": {"$toLower": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 3]},
                                        "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}
                                ]},
            "parent": {"$substr": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 2]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '.', '$$this']}}}
                    , 1, -1]},
            "site": "$site"
            },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.floor",
        "slug": "$_id.slug",
        "site": "$_id.site",
        "status": "active",
        "parent": {"name": "$_id.parent"},
        "description": "Floor"
        }
    }
]

# Create view
view_name = 'existing_devices_floors'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': existing_devices_locations_collection,
    'pipeline': floors_pipeline
})

print(f"View '{view_name}' created successfully.")

# ROOMS

# Define view pipeline
rooms_pipeline = [
    {"$group": {
        "_id": {
            "room": {"$concat": [{"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 3]},
                                                "initialValue": '',
                                                "in": {"$concat": ['$$value', '.', '$$this']}
                                                }
                                }, 1, -1]}, '.', 'LR']},
            "slug": {"$toLower": [{"$concat": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 3]},
                                        "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}},
                                        'LR']}
                                ]},
            "parent": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]},
                                                       3]
                                             },
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}
                                     }
                           }, 1, -1]
                },
            "site": "$site"
            },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.room",
        "slug": "$_id.slug",
        "site": "$_id.site",
        "status": "active",
        "parent": {"name": "$_id.parent"},
        "description": "Room"
        }
    }
]

# Create view
view_name = 'existing_devices_rooms'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': existing_devices_locations_collection,
    'pipeline': rooms_pipeline
})

print(f"View '{view_name}' created successfully.")
