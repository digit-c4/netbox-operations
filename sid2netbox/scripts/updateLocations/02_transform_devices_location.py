import pymongo
import os
import re
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

# MongoDB connection details
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')  
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
db = client['SID']

# Collections
devices_migration_collection = db['devices_migration']
devices_migration_updated_collection = db['devices_updated_location']
devices_migration_errors_collection = db['devices_parsing_errors']

# Function to transform device data
def transform_device(device):
    name = device['name']

    first_match = re.match(r'(.{4})-(.{2})-(.{4})', name)
    if first_match:
        site_name = first_match.group(1).upper()
        location_name = site_name + '..' + first_match.group(2) + '.LR'

        transformed_device = {
            "id": device['id'],
            "name": device['name'],
            "device_type": device['device_type']['id'],
            "role": device['device_role']['id'],
            "site": {
                "name": site_name
            },
            "location": {
                "name": location_name
            }
        }
        return transformed_device, None
    
    # Secondary pattern match

    second_match = re.match(r'(.{4})([a-z])(\d{2})-(.{4})', name)
    if second_match:
        site_name = second_match.group(1).upper()
        location_name = site_name + '..' + second_match.group(3) + '.LR'
        
        transformed_device = {
            "id": device['id'],
            "name": device['name'],
            "device_type": device['device_type']['id'],
            "role": device['device_role']['id'],
            "site": {
                "name": site_name
            },
            "location": {
                "name": location_name
            }
        }
        return transformed_device, None
    
    # If no pattern matches, return as error device
    error_device = {
        "id": device['id'],
        "name": device['name'],
        "device_type": device['device_type']['id'],
        "role": device['device_role']['id']
    }
    return None, error_device

# Fetch devices from the devices_migration collection
devices = list(devices_migration_collection.find())

# Transform devices and separate valid and error devices
transformed_devices = []
error_devices = []

for device in devices:
    transformed_device, error_device = transform_device(device)
    if transformed_device:
        transformed_devices.append(transformed_device)
    if error_device:
        error_devices.append(error_device)

# Insert transformed devices into the updated collection
if transformed_devices:
    devices_migration_updated_collection.insert_many(transformed_devices)
    print(f"Inserted {len(transformed_devices)} transformed devices into the devices_migration_updated collection.")
else:
    print("No valid devices to insert.")

# Insert error devices into the error collection
if error_devices:
    devices_migration_errors_collection.insert_many(error_devices)
    print(f"Inserted {len(error_devices)} error devices into the devices_migration_errors collection.")
else:
    print("No error devices to insert.")