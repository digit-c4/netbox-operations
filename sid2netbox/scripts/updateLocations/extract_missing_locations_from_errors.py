import pymongo
import os
import re

# MongoDB connection details
username = os.getenv('MONGO_USERNAME')  
password = os.getenv('MONGO_PASSWORD')  
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/")
apitoken = os.getenv('API_TOKEN')
db = client['SID']

devices_migration_errors_collection = 'devices_update_errors'

# Query to find documents with 'location' in the error field
query = {"error": {"$regex": "location"}}


# Create the parent by removing the last two characters from the location name
#parent = {"name": location_name[:-2] + "."}

# Construct the new location document
pipeline = [
        {
    "$match": {"error": {"$regex": "location"}}
    },
    {"$project": {
        "_id":0,
        "name": "$location.name",
        "slug": {"$toLower": [
                              {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]}, 
                                                               3]},
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '$$this']}
                                                   }
                           }, 1, -1]}
                              ]},
        "site": "$site.name",
        "parent": {"$substr": [
            {"$reduce": {"input": {"$slice": [{"$split": ["$location.name", "."]},2]},
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}}}
                                      , 1, -1]},
        }
    },
    {"$group": {
        "_id" : {"name": "$name", "site": "$site", "slug": "$slug", "parent": "$parent"}
        }
    },
    {"$project": {"_id": 0,
                "name": "$_id.name",
                "slug": "$_id.slug",
                "site": {
                    "name": "$_id.site"
                },
                "group": "$_id.group",
                "region": "$_id.region",
                "status": "active",
                "parent": {
                    "name": "$_id.parent"
                },
                "description": "Floor"
                }
    }
]


# Create view
view_name = 'missing_locations'

# Create new view
db.command({
    'create': view_name,
    'viewOn': devices_migration_errors_collection,
    'pipeline': pipeline
})

print("New locations have been created and inserted into missing_locations view.")
