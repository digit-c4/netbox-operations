import subprocess
import sys
import time

# Define the scripts to be called in order
scripts = [
    "sid2netbox/scripts/updateLocations/01_download_devices.py",
    "sid2netbox/scripts/updateLocations/02_transform_devices_location.py",
    "sid2netbox/scripts/updateLocations/03_create_existing_devices_locations_view.py",
    "sid2netbox/scripts/updateLocations/04_upload_missing_locations.py",
    "sid2netbox/scripts/updateLocations/05_update_devices_location.py"
]

def run_script(script_name):
    try:
        print(f"Running {script_name}...")
        subprocess.check_call([sys.executable, script_name])
        print(f"{script_name} completed successfully.\n")
    except subprocess.CalledProcessError as e:
        print(f"Error while running {script_name}: {e}")
        sys.exit(1)

if __name__ == "__main__":
    start_time = time.time()  # Start timing the operation
    for script in scripts:
        run_script(script)
    
    duration = time.time() - start_time
    print(f"All scripts executed successfully in {duration:.2f} seconds.")
