import pymongo
import os
from dotenv import load_dotenv

# Load env variables from .env file
load_dotenv() # will search for .env file in local folder and load variables 

username = os.getenv('MONGO_USERNAME')
password = os.getenv('MONGO_PASSWORD')

# Connect to MongoDB
client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/?authSource=admin")
db = client['SID']
plugs_source_collection = 'isp_plugs'
cabling_source_collection = 'isp_plugs'

# SITES - BUILDINGS

# Define view pipeline
sites_pipeline = [

    {"$group": {
        "_id": {
            "building": {"$first": "$building.value"},
            "slug": {"$toLower": [{"$first": "$building.value"}]},
        },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.building",
        "slug": "$_id.slug",
        "group": {
            "name": "ISP"
        },
        "region": {
            "name": "Italy"
        },
        "status": "active"
        }
    }
]

# Create view
view_name = 'sites'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': sites_pipeline
})

print(f"View '{view_name}' created successfully.")

# WINGS

# Define view pipeline
wings_pipeline = [

    {"$group": {
        "_id": {
            "wing": { "$substr": [
                                {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '.', '$$this']}}}
                                , 1, -1]},
            "slug": {"$toLower": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                        "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}
                                ]},
            "site": {"$first": "$building.value"},
        },
    }},

   {"$project": {
        "_id": 0,
        "name": "$_id.wing",
        "slug": "$_id.slug",
        "site": {
            "name": "$_id.site"
        },
        "status": "active",
        "description": "Wing"
        }
    }
]

# Create view
view_name = 'wings'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': wings_pipeline
})

print(f"View '{view_name}' created successfully.")

# FLOORS

# Define the first pipeline
floors_pipeline1 = [
    {
        "$group": {
            "_id": {
                "name": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 3]},
                                                "initialValue": '',
                                                "in": {"$concat": ['$$value', '.', '$$this']}
                                                }
                                }, 1, -1]},
                "site": {"$first": "$building.value"},
                "slug": {"$toLower": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 3]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}
                                            }}
                ]},
                "parent": {"$substr": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '.', '$$this']}}}
                    , 1, -1]},
            }
    }},

    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": {"name": "$_id.site"},
            "status": "active",
            "parent": {"name": "$_id.parent"},
            "description": "Floor"
        }
    }
]

# Define the second pipeline
floors_pipeline2 = [
    {
        "$group": {
            "_id": {
                "name": {"$first": "$floor.value"},
                "site": {"$first": "$building.value"},
                "slug": {"$toLower": [
                    {"$reduce": {"input": {"$slice": [{"$split": [{"$first": "$floor.value"}, "."]}, 3]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '$$this']}}}
                ]},
                "parent": {"$substr": [
                    {"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 2]},
                                "initialValue": '',
                                "in": {"$concat": ['$$value', '.', '$$this']}}}
                    , 1, -1]},
            }
    }},

    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": {"name": "$_id.site"},
            "status": "active",
            "parent": {"name": "$_id.parent"},
            "description": "Floor"
        }
    }
]

# Combine the pipelines with unionWith
floors_pipeline = floors_pipeline1 + [
    {
        "$unionWith": {
            "coll": "isp_plugs",
            "pipeline": floors_pipeline2
        }
    },
    {
        "$group": {
            "_id": {"name": "$name", "site": "$site", "slug": "$slug", "status": "$status", "parent": "$parent", "description": "$description"}
        }
    },
    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": "$_id.site",
            "status": "active",
            "parent": "$_id.parent",
            "description": "$_id.description"
        }
    }
]

# Create view
view_name = 'floors'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': floors_pipeline
})

print(f"View '{view_name}' created successfully.")

# ROOMS

# Define view pipeline
rooms_pipeline1 = [

    {
        "$group": {
        "_id": {
            "room": {"$concat": [{"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                       3]
                                             },
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}
                                     }
                           }, 1, -1]
                }, '.', 'LR']},
            "site": {"$first": "$building.value"},
            "slug": {"$toLower": [
                                {"$concat": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]}, 3]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}},
                                        'LR']}
                                ]},
            "parent": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                       3]
                                             },
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}
                                     }
                           }, 1, -1]
                }
        }}
    },

    {
        "$project": {"_id": 0,
                    "name": "$_id.room",
                    "slug": "$_id.slug",
                    "site": {
                        "name": "$_id.site"
                    },
                    "status": "active",
                    "parent": {
                        "name": "$_id.parent"
                    },
                    "description": "Room"
                  }
       }
]

rooms_pipeline2 = [

    {
        "$group": {
        "_id": {
            "room": {"$concat": [{"$first": "$floor.value"}, '.', 'LR']},
            "site":  {"$first": "$building.value"},
            "slug": {"$toLower": [
                                {"$concat": [{"$reduce": {"input": {"$slice": [{"$split": [{"$first": "$floor.value"}, "."]}, 3]},
                                            "initialValue": '',
                                            "in": {"$concat": ['$$value', '$$this']}}}, 'LR']}
                                ]},
            "parent": {"$first": "$floor.value"},
        }}
    },

    {
        "$project": {"_id": 0,
                    "name": "$_id.room",
                    "slug": "$_id.slug",
                    "site": {
                        "name": "$_id.site"
                    },
                    "status": "active",
                    "parent": {
                        "name": "$_id.parent"
                    },
                    "description": "Room"
                  }
       }
]

# Combine the pipelines with unionWith
rooms_pipeline = rooms_pipeline1 + [
    {
        "$unionWith": {
            "coll": "isp_plugs",
            "pipeline": rooms_pipeline2
        }
    },
    {
        "$group": {
            "_id": {"name": "$name", "site": "$site", "slug": "$slug", "status": "$status", "parent": "$parent", "description": "$description"}
        }
    },
    {
        "$project": {
            "_id": 0,
            "name": "$_id.name",
            "slug": "$_id.slug",
            "site": "$_id.site",
            "status": "active",
            "parent": "$_id.parent",
            "description": "$_id.description"
        }
    }
]

# Create view
view_name = 'rooms'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': rooms_pipeline
})

print(f"View '{view_name}' created successfully.")

# PLUG DEVICES

# Define view pipeline
plug_device_pipeline = [
    {
        "$project": {
            "_id": 0,
            "name": "$value",
            "device_type": {
                "model": "Network_plugs"
            },
            "role": {
                "name": "User plug"
            },
            "site": {
                "name": "ispra"
            },
            "manufacturer": "PP",
            "status": "active"
        }
    },
    { 
    "$sort": { "name": -1 } 
    },
]

# Create view
view_name = 'plug_devices'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': plug_device_pipeline
})

print(f"View '{view_name}' created successfully.")

# Network PLUG REARPORT

# Define view pipeline
plug_rearport_pipeline = [
    {
    "$project": {
        "_id": 0,
        "device": {"name": "$value"},
        "name": "$value",
        "type": "8p8c",
        "label": "User plug",
        "position": "1"
            }},
    { 
    "$sort": { "name": -1 } 
    },
    
]

# Create view
view_name = 'plug_rearport'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': plug_rearport_pipeline
})

print(f"View '{view_name}' created successfully.") 


# Network PLUG FRONTPORT

# Define view pipeline
plug_frontport_pipeline = [
    {
    "$project": {
        "_id": 0,
        "device": {"name": "$value"},
        "name": "$value",
        "rear_port": {
                "name": "$value",
                "label": "User plug",
            },
        "label": "User plug",
        "type": "8p8c",
        "position": "1"
            }},
    { 
    "$sort": { "name": -1 } 
    },
    
]

# Create view
view_name = 'plug_frontport'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': plug_frontport_pipeline
})

print(f"View '{view_name}' created successfully.") 

# PATCH PANELS

# Define view pipeline
patch_panels_pipeline = [
    # Stage 1: Transform data into Netbox API format
    {
    "$project": {
        "_id": 0,
        "name": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                       3]
                                             },
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}
                                     }
                           }, 1, -1]
                },
        "device_type": {"model": "PP_RJ45",
                        "slug": "pp_rj45"
                       },
        "role": {"name": "Patch Panel RJ45"},
        "site": {"name": {"$first": "$building.value"}},
        "building": {"$first": {"$split": ["$value", "."]}},
        "location": {"name": {"$concat": [{"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                       3]
                                             },
                                      "initialValue": '',
                                      "in": {"$concat": ['$$value', '.', '$$this']}
                                     }
                           }, 1, -1]
                }, '.', 'LR']}}
    }},
    {"$group": {"_id" : {"name": "$name",
                     "role": "$role",
                     "device_type": "$device_type",
                     "site": "$site",
                     "location": "$location"
                    }
             }
    },
       {"$project": {"_id": 0,
                   "name": "$_id.name",
                   "role": "$_id.role",
                   "device_type": "$_id.device_type",
                   "site": "$_id.site",
                   "location": "$_id.location"
                  }
       },
    { 
    "$sort": { "name": -1 } 
    },
]

# Create view
view_name = 'patch_panels'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': patch_panels_pipeline
})

print(f"View '{view_name}' created successfully.")

# PP REAR PORTS

# Define view pipeline
rear_ports_pipeline = [
    {
        "$project": {
            "_id": 0,
            "device":  {
                "name": {
                    "$substr": [
                        {
                            "$reduce": {
                                "input": {
                                    "$slice": [
                                        { "$split": ["$value", "."] },
                                        3
                                    ]
                                },
                                "initialValue": '',
                                "in": { "$concat": ["$$value", ".", "$$this"] }
                            }
                        },
                        1,
                        -1
                    ]
                }
            },
            "name": "$value",
            "type": "8p8c",
            "positions": "1"
        }
    },
    { 
        "$sort": { "name": -1 } 
    },
]

# Create view
view_name = 'pp_rear_ports'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': rear_ports_pipeline
})

print(f"View '{view_name}' created successfully.")

# PP FRONT PORTS

# Define view pipeline
front_ports_pipeline = [
    {
        "$project": {
            "_id": 0,
            "device":  {
                "name": {
                    "$substr": [
                        {
                            "$reduce": {
                                "input": {
                                    "$slice": [
                                        { "$split": ["$value", "."] },
                                        3
                                        ]},
                                "initialValue": '',
                                "in": { "$concat": ["$$value", ".", "$$this"] }
                            }
                        },
                        1,
                        -1
                    ]
                }
            },
            "name": "$value",
            "type": "8p8c",
            "rear_port": {
                "device": {"name": {
                    "$substr": [
                        {
                            "$reduce": {
                                "input": {
                                    "$slice": [
                                        { "$split": ["$value", "."] },
                                        3
                                        ]},
                                "initialValue": '',
                                "in": { "$concat": ["$$value", ".", "$$this"] }
                            }
                        },
                        1,
                        -1
                    ]
                }},
                "name": "$value"
                }
        }
    },
    { 
        "$sort": { "name": -1 } 
    }
]

# Create view
view_name = 'pp_front_ports'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': plugs_source_collection,
    'pipeline': front_ports_pipeline
})

print(f"View '{view_name}' created successfully.")

# CABLES

# Define view pipeline
cables_pipeline = [
    {
    "$match": {"$nor": [{"is connected to": None}]}
},
{"$addFields": {
        "type": "cat3",
        "SW_name": {"name": {"$toLower": {"$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$is connected to", "/"]},
                                                                           1]
                                                                 },
                                                          "initialValue": '',
                                                          "in": {"$concat": ['$$value', '.', '$$this']}
                                                         }
                                               }, 1, -1]
                                             }
                                    }},
        "SW_interface": {"$regexFind": { "input": "$is connected to", "regex": "/([0-9/])+_" }},
}},
{"$project": {
        "_id":0,
        "side_a_type": "dcim.frontport",
        "side_a_device":  {
                            "$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                                           3]
                                                                 },
                                                          "initialValue": '',
                                                          "in": {"$concat": ['$$value', '.', '$$this']}
                                                         }
                                               }, 1, -1]


                        },
         "side_a_name": "$value",
         "side_b_type": "dcim.interface",
         "side_b_device": "$SW_name.name",
         "side_b_name": {"$concat": ["GigabitEthernet", {"$trim": {"input":"$SW_interface.match", "chars": "/_"}}]},}}
]

# Create view
view_name = 'cables_network_device2pp'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': cabling_source_collection,
    'pipeline': cables_pipeline
})

print(f"View '{view_name}' created successfully.")

# CABLES part 2 - Network_plug -> PP

# Define view pipeline
cables_pipeline = [
{"$project": {
        "_id":0,
        "side_a_type": "dcim.rearport",
        "side_a_device":  {
                            "$substr": [{"$reduce": {"input": {"$slice": [{"$split": ["$value", "."]},
                                                                           3]
                                                                 },
                                                          "initialValue": '',
                                                          "in": {"$concat": ['$$value', '.', '$$this']}
                                                         }
                                               }, 1, -1]


                        },
         "side_a_name": "$value",
         "side_b_type": "dcim.rearport",
         "side_b_device": "$value",
         "side_b_name": "$value",
         }}
]

# Create view
view_name = 'cables_pp2network_plug'

# Delete view if exists
if view_name in db.list_collection_names():
    db[view_name].drop()
    print(f"Existing view '{view_name}' dropped.")

# Create new view
db.command({
    'create': view_name,
    'viewOn': cabling_source_collection,
    'pipeline': cables_pipeline
})

print(f"View '{view_name}' created successfully.")