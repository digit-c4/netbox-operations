# Table of contents
1. [General info](#general-info)
2. [One time JRC migration](#one-time-jrc-migration)
    1. [Manual download plugs from SID](#1-manual-download-plugs-from-sid)
    2. [Use source file and load MonogDB](#2-use-above-file-and-load-it-into-monogdb)
    3. [Transform and inject data using python scripts](#3-transform-and-inject-data-using-python-scripts)
        1. [Create DB Views that transforms SID data into Netbox API input](#3-transform-and-inject-data-using-python-scripts))
        2. [Trigger API calls to load Netbox](#3-transform-and-inject-data-using-python-scripts)
        3. [Manually import cables into Netbox](#3-transform-and-inject-data-using-python-scripts)
    4. [Reassign site and location of existing devices in Netbox](#4-reassign-site-and-location-of-existing-devices-in-netbox)
3. [SID2Netbox Sync Service Design](#sid2netbox-sync-service-design)
    1. [Service diagram](#service-diagram)
    2. [SID CRUD detector](#sid-crud-detector)
    3. [Data Converter Service](#data-converter-service)
    4. [Scheduler](#scheduler)
    5. [Required steps to develop services](#required-steps-to-develop-services)



# General info
This file describes step by step migration of SID network plugs database into Netbox. This migration allows operate patching and configuring network devices using only Netbox.

# One time JRC migration

## 1. Manual download plugs from SID:

    SELECT TYPE "network plug"(
    "is connected to"
    "is located in" TYPE "room" as "room"
    "is located in" TYPE "computer rack" as "rack"
    "is located in" TYPE "floor (space)" as "floor"
    "is located in" TYPE "building" (
    "is a member of" FILTER VALUE = :domain: ) as "building"
    )
    OPTION CORRECT
    TRANSFORM JSONTREE

or use stored digets under: szczark/network_plugs_in_domain
ssh vworker-dev.dev.snmc.cec.eu.int and exetute:

    vworker-dev.dev:/opt/users/petjere/convert]$ ./dieget_to_json.py -d "network_plugs_in_domain" -f "jrc_plugs.json"

## 2. Use above file and load it into MonogDB
Possible usage of Mongo deployed on host: cmdb.test.netbox.snmc.cec.eu.int - credentials to SSH can be found in [vault](https://sam-hcpvault.cec.eu.int/ui/vault/secrets/linux/kv/lab%2Fdebian%2Fcmdb.test.netbox%2Fcmdb%2Fsnet/details?namespace=EC%2FDIGIT_C4_SNET_ADMIN-PROD&version=1)
to get MongoDB credentials exetuce command: 

    export | grep MONGO

or you can use this file to compose new mongo instance on your host.

    // import SID data into mongoDB
    mongoimport --db SID --collection jrc_plugs --file /path_to_your_file/jrc_plugs.json --jsonArray --username netbox --password "xyz" --authenticationDatabase admin


## 3. Transform and inject data using python scripts:
Clone Netbox Operation repository and switch to remote SID2Netbox branch

    # Load env variables from .env file
    username = os.getenv('MONGO_USERNAME')
    password = os.getenv('MONGO_PASSWORD')

    # Connect to MongoDB
    client = pymongo.MongoClient(f"mongodb://{username}:{password}@127.0.0.1:27017/?authSource=admin")
    db = client['SID']
    plugs_source_collection = 'isp_plugs'
    cabling_source_collection = 'isp_plugs'

Go into sid2netbox folder and configure connections variables in below scripts or in .env file

    sid2netbox/scripts/01_create_all_views.py
    sid2netbox/scripts/02_migrate.py

### Prerequisites
Before running the script, ensure you have the following:

- Python 3.x installed
- Required Python packages (requests, pymongo, python-dotenv) installed. You can install them via:

```
pip install requests pymongo python-dotenv
```
- MongoDB up and running
- 'isp_plugs' collection with SID data in your MongoDB database
- API token and endpoint URL configured in the scripts or in an .env file

### 3.1. Create DB Views that transforms SID data into Netbox API input
Execute script to create input data for Netbox

    sid2netbox/scripts/01_create_all_views.py 

The following views will be created in MongoDB: 
- sites
- wings
- floors
- rooms
- plug_devices
- plug_rearport
- plug_frontport
- patch_panels
- pp_rear_ports
- pp_front_ports
- cables_network_device2pp
- cables_pp2network_plug

### 3.2. Trigger API calls to load Netbox
Execute script to load threw API earlier prepared data.

    sid2netbox/scripts/02_migrate.py 

Full execution will POST previously created MongoDB views to the following Netbox endpoints:
- devices
- device-types
- device-roles
- manufacturers
- rear-ports
- front-ports
- regions
- site-groups
- locations
- sites

And it will create two JSON files:
- cables_network_device2pp_bulk.json
- cables_pp2network_plug_bulk.json

It will take a few hours to finish. Check deadletter collections to validate possible errors - For clear run it supposed to be empty in case of errors it needs manual rollback/delete of data loaded with errors, provide fix in views logic and run again upload.

#### Running the Script
You can run the script using python 02_migrate.py followed by the operations you want to perform. The script supports running specific operations or all operations in sequence.

```
python 02_migrate.py [operation1] [operation2] ... [--batch-size N] [--skip-setup] [--skip-location] [--location-views [view1] [view2] ... ]
```

If an error occurs during a POST request, the data is saved to a deadletter collection in MongoDB for further investigation.

#### Arguments:

- Operations: The operations you want to perform. You can specify one or more operations. If you specify all, the script will run all available operations in order. Available choices = ['all', 'organisation', 'sites', 'location', 'plug_device', 'plug_rearport', 'plug_frontport', 'patch_panel', 'pp_rearport', 'pp_frontport', 'cable_network_device2pp', 'cable_pp2network_plug']

'organisation' operation creates roles, manufacturer, device types, region and sites. If you are going to run the migration script several times, or operation by operation, you only need to run the organisation step once.

- --location-views: (Required*) Specify one or more location source views to migrate in order. *\*This argument is required if 'location' operation is going to be performed.* 

- --batch-size N: (Optional) The number of items to process per batch. The default batch size is 1.

- --skip-location: (Optional) Skip creating locations. This flag can be applied when 'all' operation choice is selected. The default value is false.

#### Examples

Run All Operations with Default Batch Size:
```
python 02_migrate.py all --location-views wings floors rooms
```

Migrate only floor locations:
```
python 02_migrate.py location --location-views floors
```

Run several Specific Operations:
```
python 02_migrate.py plug_device plug_rearport
```

Run Specific Operations with Custom Batch Size:
```
python 02_migrate.py plug_device plug_frontport --batch-size 400
```

### 3.3. Manually import cables into Netbox

After fully running the `02_migrate.py` script, you will need to manually import the cables into NetBox using the two previously generated JSON files:
- cables_network_device2pp_bulk.json
- cables_pp2network_plug_bulk.json

Follow these steps to import the files using the NetBox Bulk Import UI:

1. Log in to NetBox and navigate to `Connections` > `Cables` > `Import`.
2. Open the `Upload File` tab.
3. Click `Choose File`, select `cables_network_device2pp_bulk.json`, and click `Submit`.
4. Repeat Step 3 for the second file, `cables_pp2network_plug_bulk.json`.

## 4. Reassign site and location of existing devices in Netbox

Go into sid2netbox folder and configure connections variables in below script or in .env file

    sid2netbox/scripts/03_update_existing_devices.py

Make sure which device roles should be excluded and update list in script e.g.: 
excluded_roles = ['patch-panel-rj45','user-plug','access-point','SYS']

This script performs in order the following steps:

    sid2netbox/scripts/updateLocations/01_download_devices.py
    sid2netbox/scripts/updateLocations/02_transform_devices_location.py
    sid2netbox/scripts/updateLocations/03_create_existing_devices_locations_view.py
    sid2netbox/scripts/updateLocations/04_upload_missing_locations.py
    sid2netbox/scripts/updateLocations/05_update_devices_location.py

### Prerequisites
Before running the script, ensure you have the following:

- Python 3.x installed
- Required Python packages (requests, pymongo, python-dotenv) installed. You can install them via:

```
pip install requests pymongo python-dotenv
```
- MongoDB up and running
- API token and endpoint URL configured in the scripts or in an .env file

# SID2Netbox Sync Service Design 
To achieve constant sync between SID and Netbox it requires two steps. First is service that will periodically checking SID database for changes since last check and send them into second step of the process - Conversion Service. This service will store data, apply transformation logic and update Netbox to given state.

## Service diagram
![diagram](doc/SID2Netbox_conversion_service_v0.1.drawio.svg)

> TODO: TO consider how detect deletion of data from SID and propagate it to Netbox.

## SID CRUD detector
Service dedicated to monitor and get changes made on SID since last check. Every check will create timestamp record in dedicated MongoDB collection to log runs and store aggregated information about number of records to process. Records to transform will be added or updated in main collection with updated timestamp to allow identify them.

## Data Converter Service
It's core part of the migration process. Contains MongoDB storing all data in raw format and views build on top of them containing transformation logic. Second part of engine is Python Service automating data processing. It reads MongoDB Views storing records to process and send them threw Netbox API.

## Scheduler
Scheduled cron job triggering processing pipeline ([SID CRUD detector](#sid-crud-detector) and [Data Converter Service](#data-converter-service))

## Required steps to develop services:
* ### [One time JRC migration](#one-time-jrc-migration) should be executed first
* ### Develop [SID CRUD detector](#sid-crud-detector) service
* ### Rewrite python scripts to run as a [Data Converter Service](#data-converter-service) service
* ### Implement [scheduler](#scheduler) for detector
* ### Define deployment and maintenance process