# CI-Component, Upgrade service with Netbox.

### Description

This component will help you to deploy new TAG, to target container registered into `Netbox-docker-agent` plugin, that have a selected image in it.

### Sequence Diagram, Solution.

```mermaid
sequenceDiagram
  %%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Service upgrade with Netbox
  %%%%%%%%%%%%%%%%%%%%%%%%%%

  title Service Upgrade with Netbox

  Actor p0 as Release Manager
  participant gitr as Gitlab Runner
  participant serv as Service
  participant cmdb as CMDB
  participant netbox as Netbox[n]
  participant agent as "Host Netbox Docker Agent[n][m]"
  participant docker as "Host Docker Service[n][m]"
  participant cont as "Container Upgrade Agent[n][m]"

  p0 ->> gitr: New Release / Tag
  gitr ->> gitr: Scan new Docker Image
  gitr ->> gitr: Deliver New Docker Image
  gitr ->> serv : New Docker Image delivered
  serv ->> gitr: Ok
  serv ->> cmdb: Get Netbox Urls With App Name And Location
  cmdb ->> serv: Netbox Urls

  loop: For each Netbox Urls
    serv ->> netbox: Get Docker Hosts with Image
    netbox ->> serv: Docker Hosts

    loop: For each Docker Host
      serv ->> netbox: Create New Docker Image Record on Docker Host
      netbox ->> serv: Ok
      netbox ->> agent: New Docker Image Record Created
      agent ->> netbox: Ok
      agent ->> docker: Pull new Docker Image
      docker ->> docker: Pull Docker Image
      docker ->> agent: Ok
      agent ->> netbox: Create Journal entry
      netbox ->> agent: Ok

      alt: If Containers with Image must be upgraded
        netbox ->> cont: New Docker Image pulled
        cont ->> netbox: Get Containers to upgrade on Host
        netbox ->> cont: Containers

        loop: For each Container
          cont ->> netbox: Update Container Record Image
          netbox ->> cont: Ok
          cont ->> netbox: Recreate Container Operation
          netbox ->> agent: Recreate Container Operation
          agent ->> docker: Doing Recreate Operation
          docker ->> agent: Done
          agent ->> netbox: Create Journal entry
          netbox ->> agent: Ok
        end
      end
    end
  end
```

### Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: code.europa.eu/digit-c4/netbox/netbox-backup-agent/deploy_img@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This adds a job called `upload-img-job` to the pipeline.

### Requirements

This job needs a CI file with the name "NETBOX_LIST". JSON Format. e.g.:

```
 [
  {
    "netbox": "http://netbox.domain.com:8080",
    "token": "somenetboxtoken123456789",
    "description": "optinal_description_value"
  },
  {
    "netbox": "http://netbox.domain2.com:8082",
    "token": "somenetboxtoken123456789",
    "description": "testing_lab2"
  }
 ]
```

It will be use to decide what Netbox with `netbox-docker-plugin` connect to.

### Other optional Inputs

Copied from component yaml file:

```
    stage:
      default: deploy
      description: 'Defines the build stage'
    REGISTRY_CHECK:
      default: https://code.europa.eu:4567/v2/
      description: "To check that a regitry exist on target VM before upload image."
    REGISTRY_IMAGE_UPLOAD:
      default: $CI_REGISTRY_IMAGE
      description: "Image to upload"
    COMMIT_TAG_UPLOAD:
      default: $CI_COMMIT_TAG
      description: 'Defines TAG to use to deploy/upload. Two options, `$CI_COMMIT_TAG` or `test-deploy`'
    DOCKER_HOST:
      default: "all"
      description: "Docker host to deploy to, leave empty to go through all of them."
    NETBOX_URL:
      default: "http://netbox.domain2.com:8080"
      description: "For testing purposes. Your target Netbox."
```

### Use basic example

```
stages:
  - deploy

include:
  - component: code.europa.eu/digit-c4/netbox-operations/deploy_img@main
    inputs:
      stage: "deploy"
```

### Complex example to create CI form and allow manual tests.

```
workflow:
  rules:
    # To run, just, if manually triggered from CI-forms
    - if: ($COMMIT_TAG_DEPLOY != "" && $COMMIT_TAG_DEPLOY != $CI_COMMIT_TAG)

variables:
  COMMIT_TAG_DEPLOY:
    value: ""
    description: "Select tag image to deploy to a Netbox."
  NETBOX_URL:
    value: "http://netbox.domain.com:8080"
    options:
      - "http://netbox.domain.com:8080"
      - "http://netbox.domain2.com:8000"
    description: "Select your target Netbox from drop menu, or write your own, it will work as long is on the fetched list."
  DOCKER_HOST_TARGET:
    value: "all"
    description: "Select target Docker Host if needed."

stages:
  - deploy

include:
  #- component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/deploy_img@$CI_COMMIT_SHA
  - component: code.europa.eu/digit-c4/netbox-operations/deploy_img@main
    inputs:
      stage: "deploy"
      COMMIT_TAG_UPLOAD: $COMMIT_TAG_UPLOAD
      NETBOX_URL: $NETBOX_URL
      DOCKER_HOST: $DOCKER_HOST_TARGET
```
