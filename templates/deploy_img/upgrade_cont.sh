#!/bin/bash
#
#
# To execute this script
# ./upgrade_cont.sh -h
# ./upgrade_cont.sh -l <netbox_url> -t <netbox_token> -H <docker_host>
#
#

# Default values
# Your target Netbox. Use with CI forms to manually target one single Netbox.
NETBOX_URL="http://lab.testing.com:8080"
NETBOX_TOKEN="b63faketoken1234"
# Docker host to deploy to, leave empty to go through all of them.
DOCKER_HOST="all"
# To check that a regitry exist on target VM before deploy image.
REGISTRY_CHECK="https://code.europa.eu:4567/v2/"
# Image to deploy
REGISTRY_IMAGE_DEPLOY="code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent"
# Defines TAG to use to deploy/upload.
COMMIT_TAG_DEPLOY="v1.2.1"
# Verbosity level, hide or show extra output.
VERBOSITY="false"

# working directory
DIR2SAVE="${0%/*}/"
WORKDIR=$DIR2SAVE

# load libraries
. ${WORKDIR}lib_cont.sh

# requirements
require(){
        command -v "$1" &> /dev/null || { echo "Error: '$1' is not installed or not in the PATH."; exit 1; }
}
require "jq"		#  apt install jq
require "docker"

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-s SEC]"
  cat <<'  EOF'
  Options:
    -h,	Display this help message
    -l, NETBOX_URL
    -t, NETBOX_TOKEN
    -H, DOCKER_HOST
    -r, REGISTRY_CHECK
    -i, REGISTRY_IMAGE_DEPLOY
    -T, COMMIT_TAG_DEPLOY
    -v, VERBOSITY
    -d,	Full path Directory to save files
  EOF
  exit 0
}

# Parse command-line options
while getopts ":hl:t:H:r:i:T:v:d:" opt; do
  case $opt in
    h) help ;;
    l) NETBOX_URL=$OPTARG ;;
    t) NETBOX_TOKEN=$OPTARG ;;
    H) DOCKER_HOST=$OPTARG ;;
    r) REGISTRY_CHECK=$OPTARG ;;
    i) REGISTRY_IMAGE_DEPLOY=$OPTARG ;;
    T) COMMIT_TAG_DEPLOY=$OPTARG ;;
    v) VERBOSITY=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
#if [[ $1 == '' ]]; then help ; fi
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi

# print variables
echo "Image to deploy, ${REGISTRY_IMAGE_DEPLOY}:${COMMIT_TAG_DEPLOY}"
# check if image exist on registry
#docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY;
#if [[ $(docker manifest inspect ${REGISTRY_IMAGE_DEPLOY}:${COMMIT_TAG_DEPLOY} | jq '.config.size') > 0 ]];
#	then echo "Image exists for download.";
#	else echo "Can not find image on docker registry." && exit 1;
#fi
#docker logout $CI_REGISTRY;

mkdir -p ${DIR2SAVE}tmp/

# upgrade VMs connected to docker plugin on a netbox
upgrade_all_hosts_func ${DOCKER_HOST} ${REGISTRY_IMAGE_DEPLOY} ${COMMIT_TAG_DEPLOY} ${REGISTRY_CHECK} ${VERBOSITY}

rm -r "${DIR2SAVE}tmp/"


exit 0
