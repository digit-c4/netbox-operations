
# curl GET info from netbox
#  syntax: function_name 1-GET/POST/PATH/DELETE 2-api_enpoint 3-json_content 4-output_file
#  cmd e.g.:  curl_netbox_func "POST" "$[[ inputs.NETBOX_URL ]]/api/plugins/docker/hosts/" '{}' "/tmp/res.json"
curl_netbox_func () {
	# print command && squeeze-repeats
	echo "curl -s -X ${1} ${NETBOX_URL}/api/${2} -d ${3}" |tr -s ' '
	# execute
	curl -s -X ${1}   "${NETBOX_URL}/api/${2}" \
	-H 'accept: application/json; indent=4' \
	-H 'Content-Type: application/json' \
	-H "Authorization: Token ${NETBOX_TOKEN}" \
	-d "${3}" \
	> "${4}"
}

# recolect image info
#  syntax: function_name 1-NETBOX_PLUGIN_HOST_ID 2-IMAGE:TAG 3-output_file 4-verbosity
#  cmd e.g.: fetch_img_func ${NETBOX_PLUGIN_HOST_ID} $[[ inputs.REGISTRY_IMAGE_DEPLOY ]]:$[[ inputs.COMMIT_TAG_DEPLOY ]] img.json $[[ inputs.VERBOSITY ]]
fetch_img_func () {
	# curl GET info from netbox
	curl_netbox_func "GET" "plugins/docker/hosts/" '{}' "${DIR2SAVE}tmp/res.json"

	# recolect image info
	#e.g.: curl -X 'GET'   "mylab.d.publicvm.com:8082/api/plugins/docker/hosts/" -H 'accept: application/json; indent=4' -H "Authorization: Token b63.." | jq '.results[] | select(.id | contains(19)) | .images[] | select(.display | contains("code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent:v1.2.1") ) | .size'
	# save image JSON.
	cat "${DIR2SAVE}tmp/res.json" | jq '.results[] | select(.id | contains('${1}')) | .images[] | select(.display | contains("'${2}'") ) ' > "${DIR2SAVE}tmp/${3}"

	# print extra output
	if [[ ${4} == true ]]; then cat ${DIR2SAVE}tmp/${3}; fi
}

# wait for image to get pulled
#  syntax: function_name 1-NETBOX_PLUGIN_HOST_ID 2-IMAGE 3-TAG 4-verbosity
#  cmd e.g.: wait_img_pull_func ${NETBOX_PLUGIN_HOST_ID} $[[ inputs.REGISTRY_IMAGE_DEPLOY ]] $[[ inputs.COMMIT_TAG_DEPLOY ]] $[[ inputs.VERBOSITY ]]
wait_img_pull_func () {
	# wait until image is pulled
	for ((i=1; i<=10; i++)); do

		sleep 20
		# create file "img.json" with image's info.
		fetch_img_func ${1} ${2}:${3} img.json ${4}

		# set variables
		imgSize=$(cat "${DIR2SAVE}tmp/img.json" | jq '.size')
		imgId=$(cat "${DIR2SAVE}tmp/img.json" | jq '.id')
		echo "imgSize: ${imgSize}, imgId: ${imgId}"

		# check that image has been uploaded
		if [[ ${imgSize} > 0 ]];
		  then echo "image uploaded."; break;
		  else echo "image not yet uploaded, sleep for 20 seconds..";
		fi
		if [[ $i == 10 ]];
		  then echo "It seems not able to upload image, host_id: ${1}."; exit 1;
		fi
	done
}

# wait for container to restart
#  syntax: function_name 1-Container_id
#  cmd e.g.: wait_cont_restart_func ${contId}
wait_cont_restart_func () {
	# wait until container is restarted
	for ((i=1; i<=10; i++)); do
		# curl recolect container info
		#e.g.: curl -X 'GET' "mylab.d.publicvm.com:8082/api/plugins/docker/containers/" -H 'accept: application/json; indent=4' -H "Authorization: Token b63.." -H 'Content-Type: application/json' | jq '.results[] |select(.image.name | contains("code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent"))'
		sleep 20 && curl_netbox_func "GET" "plugins/docker/containers/${1}/" '{}' "${DIR2SAVE}tmp/res_container_updated.json"
		# save container JSON.
		# cat "${DIR2SAVE}tmp/res_container_updated.json" | jq '.results[] |select(.image.name | contains("'$[[ inputs.REGISTRY_IMAGE_DEPLOY ]]'") ) ' > "${DIR2SAVE}tmp/cont.json"
		# set variables
		contOperation=$(cat "${DIR2SAVE}tmp/res_container_updated.json" | jq '.operation')
		contImgId=$(cat "${DIR2SAVE}tmp/res_container_updated.json" | jq '.image.id')
		echo "contOperation: ${contOperation}, contImgId: ${contImgId}"

		# check that container has been recreated
		if [[ ${contOperation:1:4} == "none" ]];
		  then echo "container recreated and ready."; break;
		  else echo "container not yet recreated, sleep for 20 seconds..";
		fi
		if [[ $i == 10 ]];
		  then echo "It seems not able to recreate container, cont_id: ${1}."; exit 1;
		fi
	done
}

# push image into netbox
#  syntax: function_name 1-NETBOX_PLUGIN_HOST_ID 2-IMAGE 3-TAG 4-REGISTRY 5-output_file 6-verbosity
#  cmd e.g.: push_img_func ${NETBOX_PLUGIN_HOST_ID} $[[ inputs.REGISTRY_IMAGE_DEPLOY ]] $[[ inputs.COMMIT_TAG_DEPLOY ]] $[[ inputs.REGISTRY_CHECK ]] res2.json $[[ inputs.VERBOSITY ]]
push_img_func () {

	# create file "img.json" with image's info.
	fetch_img_func ${1} ${2}:${3} img.json ${6}

	# set variables
	imgSize=$(cat "${DIR2SAVE}tmp/img.json" | jq '.size')
	imgId=$(cat "${DIR2SAVE}tmp/img.json" | jq '.id')
	echo "imgSize: ${imgSize}, imgId: ${imgId}"

	# check if image already exist.
	if   [[ ${imgSize} > 0 ]]; then echo "WARNING: Good image already uploaded.";
	elif [[ ${imgId} != '' ]]; then echo "WARNING: Bad  image already uploaded. Try to remove that manually."; exit 1;
	else

		# curl POST image to netbox
		curl_netbox_func "POST" "plugins/docker/images/" "{ \
		  \"name\": \"${2}\", \
		  \"version\": \"${3}\", \
		  \"host\": \"${1}\", \
		  \"registry\": {\"serveraddress\": \"${4}\"} \
		}" "${5}"

		# print extra output
		if [[ ${6} == true ]]; then cat ${5}; fi

		# wait until image is pulled
		wait_img_pull_func ${1} ${2} ${3} ${6}

	fi
}

# containers to update
#  syntax: function_name 1-REGISTRY_IMAGE 2-img_id_to_use
#  cmd e.g.: update_cont_func $[[ inputs.REGISTRY_IMAGE_DEPLOY ]] ${imgId} ${COMMIT_TAG_DEPLOY}
update_cont_func () {
	# curl recolect container info
	#e.g.: curl -X 'GET' "mylab.d.publicvm.com:8082/api/plugins/docker/containers/" -H 'accept: application/json; indent=4' -H "Authorization: Token b63.." -H 'Content-Type: application/json' | jq '.results[] |select(.image.name | contains("code.europa.eu:4567/digit-c4/netbox/netbox-backup-agent"))'
	curl_netbox_func "GET" "plugins/docker/containers/" '{}' "${DIR2SAVE}tmp/res_containers.json"
	# save container to use to JSON.
	cat "${DIR2SAVE}tmp/res_containers.json" | jq '[.results[] |select(.image.name | contains("'${1}'") ) ]' > "${DIR2SAVE}tmp/containers_to_update.json"

	# Check how many containers need to be updated
	container_count=$(jq '. | length' ${DIR2SAVE}tmp/containers_to_update.json)
	if [[ $container_count -eq 0 ]]; then
	  echo "No containers need an update."
	else
	  echo "$container_count containers need to be updated."

	  jq -c '.[]' "${DIR2SAVE}tmp/containers_to_update.json" | while read -r container; do
		# set variables
		contName=$(echo $container | jq '.name')
		contId=$(echo $container | jq '.id')
		contImgId=$(echo $container | jq '.image.id')
		echo "Processing container: ${contName}, contId: ${contId}, contImgId: ${contImgId}"

		# check variables results
		if [[ ${contImgId} == null || ${contId} == null || ${contName} == null || ${contId} == '' ]]; then echo "could not find some container info."; exit 1; fi

		# check that container is not using new image already
		if [[ $contImgId == ${2} ]]; then
		  echo "Container $contName is already using the new image. Skipping..."
		  continue
		fi

		# check if new tag is bigger that older tag
		current_tag=$(echo $container | jq -r '.image.version')
		new_tag=${COMMIT_TAG_DEPLOY}

		# If current tag is invalid or missing, force update
		if [[ -z "$current_tag" || "$current_tag" == "null" ]]; then
		  echo "Current tag is invalid or missing. Proceeding with update..."
		elif [[ "$current_tag" == "latest" ]]; then
		  echo "Container $contName is using the 'latest' tag. Skipping update..."
		  continue
		else
		  # Compare versions
		  if [[ $(echo -e "$current_tag\n$new_tag" | sort -V | tail -n 1) != "$new_tag" ]]; then
		    echo "New tag ($new_tag) is not greater than current tag ($current_tag). Skipping update..."
		    continue
		  fi
		fi

		echo "New tag ($new_tag) is greater than current tag ($current_tag). Proceeding with update..."

		echo "Recreating container $contName with new image..."

		# curl update&&recreate container,  TO-DO: might need to include external ports!!!!
		#e.g.: curl -X 'PATCH' "mylab.d.publicvm.com:8082/api/plugins/docker/containers/4/" -H 'accept: application/json; indent=4' -H "Authorization: Token b6.." -H 'Content-Type: application/json' -d "{ \"name\": \"netbox-gxy-bckp-agt-0\", \"image\": \"11\", \"operation\": \"recreate\" }"
		curl_netbox_func "PATCH" "plugins/docker/containers/${contId}/" \
		  "{ \"name\": ${contName}, \"image\": \"${2}\", \"operation\": \"recreate\" }" "${DIR2SAVE}tmp/res_update.json"

		# wait until container is restarted
		wait_cont_restart_func ${contId}
	  done
	fi
}

# update containers on target VM(host)
#  syntax: function_name 1-DOCKER_PLUGIN_HOST_ID 2-REGISTRY_IMAGE 3-TAG 4-REGISTRY 5-VERBOSITY
#  cmd e.g.: upgrade_host_func ${str_host_id} $[[ inputs.REGISTRY_IMAGE_DEPLOY ]] $[[ inputs.COMMIT_TAG_DEPLOY ]] $[[ inputs.REGISTRY_CHECK ]] $[[ inputs.VERBOSITY ]]
upgrade_host_func () {
	# VMs with a netbox (host_id).
	NETBOX_PLUGIN_HOST_ID="${1}"
	echo "netbox_plugin_host_id to connect to: $NETBOX_PLUGIN_HOST_ID"

	# curl POST image to netbox
	push_img_func ${NETBOX_PLUGIN_HOST_ID} ${2} ${3} ${4} res_push.json ${5}

	# create file "img.json" with image's info.
	fetch_img_func ${1} ${2}:${3} img.json ${5}
	imgId_to_use=$(cat "${DIR2SAVE}tmp/img.json" | jq '.id')

	# update containers
	update_cont_func ${2} ${imgId_to_use} ${3}
}

# update all VMs(hosts) on a netbox, or just one.
#  syntax: function_name 1-host_to_upgrade("all") 2-REGISTRY_IMAGE 3-TAG 4-REGISTRY 5-VERBOSITY
#  cmd e.g.: upgrade_all_hosts_func  $[[ inputs.DOCKER_HOST ]] $[[ inputs.REGISTRY_IMAGE_DEPLOY ]] $[[ inputs.COMMIT_TAG_DEPLOY ]] $[[ inputs.REGISTRY_CHECK ]] $[[ inputs.VERBOSITY ]]
upgrade_all_hosts_func () {
	# curl GET info from netbox
	curl_netbox_func "GET" "plugins/docker/hosts/" '{}' "${DIR2SAVE}tmp/res.json"
	host_counter=0

	# iterate over VMs with a Netbox inside.
	for str_host_id in $(cat "${DIR2SAVE}tmp/res.json" | \
	jq ".results[] | select(.images[].name | contains( \"${2}\" )) | .id" | \
	uniq)
	do
	  # get host name into variable
	  str_host_display=$(cat "${DIR2SAVE}tmp/res.json" | \
	    jq -r ".results[] | select(.id==${str_host_id}) | .display")
	  echo "Host: ${str_host_display}, to run with ${1}."

	  if [[ ${1} != "all" ]] ; then
	    # just one host to upgrade
	    if [[ ${1} == ${str_host_display} ]] ; then
	      echo "One single host selected.";
	      host_counter=1
	      upgrade_host_func ${str_host_id} ${2} ${3} ${4} ${5}
	    fi
	  else
	    # Upgrade all the hosts
	    upgrade_host_func ${str_host_id} ${2} ${3} ${4} ${5}
	  fi
	done
	# fail if wrong host name.
	if [[ ${host_counter} < 1 && ${1} != "all"  ]] ; then echo "No container to upgrade found with that host name." ; exit 1; fi
}
