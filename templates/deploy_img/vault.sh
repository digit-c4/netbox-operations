#!/usr/bin/bash

# This script accepts three parameters: role_id, secret_id and a path
# Then it uses the role_id and secret_id to login to Vault and reads
# the secret at the given path.
#
# Requires: env variables VAULT_NAMESPACE and VAULT_ADDR need to be set


# Check if the number of arguments provided is exactly 3
if [ "$#" -ne 3 ]; then
    echo "Syntax: $0 <role_id> <secret_id> <path>"
    exit 1
fi

# Check needed env variables are defined
if [ -z "${VAULT_NAMESPACE}" ] || [ -z "${VAULT_ADDR}" ]; then
    echo "Error: environment variables VAULT_NAMESPACE and VAULT_ADDR need to be set."
    exit 1
fi

# Give meaningful names to input parameters
ROLE_ID=$1
SECRET_ID=$2
SECRET_PATH=$3

# Get a token from Vault using the role_id and secret_id
echo "LOGGING TO VAULT WITH APPROLE ----------------------------"
echo
JSON_DATA="{\"role_id\":\"${ROLE_ID}\",\"secret_id\":\"${SECRET_ID}\"}"
TEMP_TOKEN=`curl -s \
 -H X-Vault-Namespace:${VAULT_NAMESPACE} \
 -X POST \
 --tlsv1.2 \
 --data "${JSON_DATA}" \
 ${VAULT_ADDR}/v1/auth/approle/login |
 jq '.auth.client_token' | \
 sed 's/\"//g' `
echo "Received token $TEMP_TOKEN"
echo

# Read the secret at the given path from the Vault
echo "READING SECRET -------------------------------------------"
echo "/usr/bin/curl -s \\"
echo "    -H X-Vault-Namespace:${VAULT_NAMESPACE} \\"
echo "    --header X-Vault-Token:(TOKEN HERE) \\"
echo "    --request GET \\"
echo "    --tlsv1.2 \\"
echo "    ${VAULT_ADDR}/v1${SECRET_PATH} | \\"
echo "    jq '.'"

/usr/bin/curl -s \
 -H X-Vault-Namespace:${VAULT_NAMESPACE} \
        --header X-Vault-Token:${TEMP_TOKEN} \
        --request GET \
 --tlsv1.2 \
 ${VAULT_ADDR}/v1${SECRET_PATH} | \
 jq '.'
 
