# CI-Component, Action through Netbox API.

### Description

This component will help you to modify a target Netbox through its API.

Helping you to execute the following role, with just one-click from CI-forms.

[../../playbooks/roles/netbox_api/README.md](../../playbooks/roles/netbox_api/README.md)

### Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: code.europa.eu/digit-c4/netbox-operations/netbox_api@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This adds a job called `action-tag-job` to the pipeline.

### Requirements

This job needs a CI file with the name "NETBOX_LIST". JSON Format. e.g.:

```
 [
  {
    "netbox": "http://netbox.domain.com:8080",
    "token": "somenetboxtoken123456789",
    "description": "optinal_description_value"
  },
  {
    "netbox": "http://netbox.domain2.com:8082",
    "token": "somenetboxtoken123456789",
    "description": "testing_lab2"
  }
 ]
```

It will be use to read token from it.

### Other optional Inputs

Copied from component yaml file:

```
    stage:
      default: action_tag
      description: 'Defines the build stage'
    ACTION_TAG:
      default: ""
      description: 'Defines TAG to use to deploy/upload.'
    NETBOX_URL:
      default: "http://netbox.domain2.com:8080"
      description: "Your target Netbox. Use with CI forms to manually target one single Netbox."
    DOCKER_HOST:
      default: ""
      description: "Your target Host. Use with CI forms to manually target one single Host."
```

### Example to create CI form and allow manual tests.

```
workflow:
  rules:
    # if manually triggered from CI-forms
    - if: ($ACTION_TAG != "" && $NETBOX_URL != "" && $DOCKER_HOST != "")

variables:
  ACTION_TAG:
    value: ""
    options:
      - ""
      - "refresh_host"
      - "install_traefik"
      - "install_bckp_agt"
      - "install_bckp_agt_with_traefik"
    description: "Action tag for playbook."
  NETBOX_URL:
    value: ""
    options:
      - ""
      - "http://netbox.domain.com:8080"
      - "http://netbox.domain2.com:8000"
    description: "Select your target Netbox from drop menu, or write your own, it will work as long is on the fetched list."
  DOCKER_HOST:
    value: ""
    description: "Select docker-host-name to create accion."

stages:
  - action_tag

include:
  - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/netbox_api@$CI_COMMIT_SHA
  #- component: code.europa.eu/digit-c4/netbox-operations/netbox_api@main
    inputs:
      stage: "action_tag"
      ACTION_TAG: $ACTION_TAG
      NETBOX_URL: $NETBOX_URL
      DOCKER_HOST: $DOCKER_HOST
```
