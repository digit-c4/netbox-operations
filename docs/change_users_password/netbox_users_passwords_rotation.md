# Netbox Users' Passwords Rotation

Version: 1.0.0, 2024-04-10

## Context

As part of DIGIT C4's infrastructure modernization program, we have decided to deploy a Netbox instance for each Bubble[^1].

With regard to security, Netbox integrates [user authentication mechanisms user authentication](https://docs.netbox.dev/en/stable/administration/authentication/overview/).

### Problems description

At the time of writing, the authentication management service had not yet been defined.

Since the bubbles are isolated, we can't easily consume a centralized source of identity with the Netbox instances deployed in them.

Given these difficulties, we decided to setup an unique user and password using the local authentication mechanism of Netbox.

## Solution Description

Due to this lack of security:

* One Netbox user and password are share with every body
* Password was never change and cannot respect European Commission compliancies.

The first point will be solve by the authentication management service definition and deployment. We decide to solve the second by a password rotation process.

We describe bellow the process of one iteration of the password rotation:

![Process that describe Netbox Users' passwords
](./netbox_users_passwords_rotation.png)

1. We provide a list of Netbox usernames we want to change password
2. With the module `hashivault_read` (from [Hashivault
   modules](https://terryhowe.github.io/ansible-modules-hashivault/modules/list_of_hashivault_modules.html))
   we retrieve the Netbox User's password that have the right to change Netbox
   Users' password (Netbox Admin).
3. Via Netbox API webservice we create a Token for Netbox Admin
4. We retrieve the ID of all Netbox Users we want to change password
5. For each Users IDs we:
    - generate a new password that match passwords compliancy (32 characters, with ASCII letters, punctuation and digits)
    - store the new password in Vault in place of the old password
    - change the User password with Netbox API webservice

## Operations

Netbox password rotation will be executed each week for each bubble. The
[Ansible playbook that implement the
process](../../playbooks/change_users_password/) will place passwords on an
HashiCorp Vault at the key store `cmdb` and this namespace pattern:
`{bubble_name}/netbox/{netbox_fqdn}/all/{username}`. Finally the key `password`
wil contain the password of `{username}`:

The placeholder definition of the namespace pattern are:

* `{bubble_name}`: the name of the Bubble where find the Netbox instance
* `{netbox_fqdn}`: the FQDN of the Netbox instance
* `{username}`: the password username

[^1]: Bubble is an isolated and autonomous infrastructure that expose DIGIT C4 services to customer and operations.
