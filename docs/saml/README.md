# Netbox & SAML Integration

In this document, we will show how we can setup an environment that integrates Netbox with SAML. Security Assertion Markup Language (SAML) is a protocol that allow integrators to delegate their users identification to an _Identity Provider_ (IdP). [And there are a lot of advantages to integrate an IdP with SAML](https://saml.xml.org/advantages-saml).

As the European Commission standard, we will use [EULogin](https://webgate.ec.europa.eu/cas/about.html) as IdP.

We will call the _Service Provider_ (SP) the entity that serves the service user call for and that receives and accepts _authentication assertions_ in conjunction with the IdP. The IdP manages a _Single SignOn_ (SSO) that will receive authentification request from the SP and return authentication assertions contained in a SAML Response to the _Assertion Consumer Service_ (ACS) of the SP.

## Integration Overview

We use the Reverse Proxy Service to integrate SAML.

![Netbox SAML Integration](./assets/saml-dynamic.png)

As shown in the previous diagram. We provide two accesses to Netbox:

 - One with SAML integration
 - one without SAML integration and only the Netbox builtin authentication system

The 2 reverse proxy servers are accessible via two different Full Qualified Domain Name (FQDN).

## Nginx+ Configuration

[The RPS Squad wrote a document about the NGINX+ configuration.](https://code.europa.eu/digit-c4/rps/service-entrypoint/-/wikis/HowTo:%20Create%20a%20mapping%20with%20SAML)

In a nutshell, we expect, at the Netbox level, to [receive a `Remote-User` HTTP header](https://docs.djangoproject.com/en/5.1/ref/contrib/auth/#django.contrib.auth.backends.RemoteUserBackend). This header will contain the `uid` of the user provided by EULogin in the [NameID SAML Assertion Subject parameter in the SAML Response](http://docs.oasis-open.org/security/saml/Post2.0/sstc-saml-tech-overview-2.0-cd-02.html#4.4.2.Assertion,%20Subject,%20and%20Statement%20Structure|outline).

## Netbox Configuration

We can configure the Netbox Instance by [setting with environment variables some remote authentication directive](https://netboxlabs.com/docs/netbox/en/stable/configuration/remote-authentication/):

* `REMOTE_AUTH_ENABLED` (boolean, default `false`): set to `true`. Enable the remote authentication.
* `REMOTE_AUTH_AUTO_CREATE_USER` (boolean, default `false`): set to `false`. Disable the auto creation of user.
* `BANNER_LOGIN` (text, default `""`): add some text on the login page. Like: "Your EC login is not found on this Netbox Instance. Please contact your Administrator if you want to have granted access."

Be sure that the Netbox Instance was provisionned with the right user account. If identified user by Eulogin was forwarded to the Netbox Instance and this user was not registered with its `uid`, then the user will be forwarded on the Netbox Instance login page.

## Security Consideration

As the Netbox instance was configured to trust the Remote-User header, this means that the direct access with the RPS without SAML integration is sensible to a security vulnerability. [As described by RPS documentation](https://code.europa.eu/digit-c4/rps/service-entrypoint/-/wikis/HowTo:%20Create%20a%20mapping%20with%20SAML#warning-lock-security-concerns-about-dual-access) we need to remove (or set to an empty string) the HTTP header `Remote-User`.
