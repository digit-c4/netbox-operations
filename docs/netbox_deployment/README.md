# Netbox Deploiement Process

Netbox is a standard web application connected to a database. In addition to offering data persistence, Netbox uses [pub/sub](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern) functionality from Redis to implement a [webhook mechanism](https://en.wikipedia.org/wiki/Webhook).

![Netbox Standard Installation](./assets/Netbox%20Container%20Diagram.png)

Netbox, Netbox Housekeeping, Netbox worker, Redis, Redis cache and PostgreSQL represents the Netbox Stack. This stack The is created using the Docker Service.

## Stack Components

* **Netbox**: container that expose to Customer via HTTP the GUI and the webservice of Netbox. It uses [Nginx Unit](https://unit.nginx.org/) for handling HTTP request and interface with [WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) calling convention in order to forward request to Netbox Python application. It consumes _PostgreSQL_ for data persistance, _Redis Cache_ for in-memory storage and _Redis_ for publishing jobs. **Its the only exposed enpoint for Customers.**

  Docker Image: [code.europa.eu:4567/digit-c4/netbox-plugins](https://code.europa.eu/digit-c4/netbox-plugins/container_registry/90)

* **Netbox Worker**: background worker that subscribe to _Redis_ in order to mainly handle [webhook execution](https://netboxlabs.com/docs/netbox/en/stable/integrations/webhooks/).

  Docker Image: [code.europa.eu:4567/digit-c4/netbox-plugins](https://code.europa.eu/digit-c4/netbox-plugins/container_registry/90)

* **Netbox Housekeeping**: provide a scheduled [Housekeeping service](https://netboxlabs.com/docs/netbox/en/stable/administration/housekeeping/)

  Docker Image: [code.europa.eu:4567/digit-c4/netbox-plugins](https://code.europa.eu/digit-c4/netbox-plugins/container_registry/90)

* **Redis**: use for the [Redis' pub/sub servive](https://redis.io/glossary/pub-sub/). Consumed by _Netbox_ and _Netbox Worker_ for trigger webhooks when data change are requested by Customer.

  Docker Image: [docker.io/redis:7-alpine](https://hub.docker.com/_/redis)

* **Redis Cache**: in-memory storage consumed by _Netbox_ for caching purpose.

  Docker Image: [docker.io/redis:7-alpine](https://hub.docker.com/_/redis)

* **PostgreSQL**: [Relational Database](https://www.postgresql.org/) that provide for _Netbox_'s data model a persistance layer. Like all web application connected to a database, we need to pay attention to the configuration of this service according to the characteristics of the virtual machine which host the Netbox Stack.

  Docker Image: [docker.io/postgres:15-alpine](https://hub.docker.com/_/postgres)

## Docker Compose

[Docker Compose](https://docs.docker.com/compose/) is a tool for automating the implementation of several containers that make up an application (or application stack). Containers can share the same network, mount volumes and start up thanks to a configuration describing how the application stack is composed.

Cette configuration prend la forme d'un fichier au format YAML ou JSON (https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json)

### Installation Process

Here's an example of how to create a Netbox stack using a Docker Compose configuration:

```yaml
services:
    netbox:
        image: code.europa.eu:4567/digit-c4/netbox-plugins:${TAG}
        ports:
            - 8080:8080
        depends_on:
            postgres:
                condition: service_healthy
            redis:
                condition: service_started
            redis-cache:
                condition: service_started
        user: 'unit:root'
        healthcheck:
            interval: 60s
            start_period: 60s
            start_interval: 5s
            retries: 5
            test: "curl -f http://localhost:8080/api/ || exit 1"
        env_file: env/netbox.env
        volumes:
            - ./data:/input_data
            - netbox-media-files:/opt/netbox/netbox/media
            - netbox-reports-files:/opt/netbox/netbox/reports
            - netbox-scripts-files:/opt/netbox/netbox/scripts
    netbox-worker:
        image: code.europa.eu:4567/digit-c4/netbox-plugins:${TAG}
        depends_on:
            netbox:
                condition: service_healthy
        command:
            - /opt/netbox/venv/bin/python
            - /opt/netbox/netbox/manage.py
            - rqworker
        env_file: env/netbox.env
    netbox-housekeeping:
        image: code.europa.eu:4567/digit-c4/netbox-plugins:${TAG}
        depends_on:
            netbox:
                condition: service_healthy
        command:
            - /opt/netbox/housekeeping.sh
        env_file: env/netbox.env
    postgres:
        image: docker.io/postgres:15-alpine
        env_file: env/postgres.env
        healthcheck:
            interval: 60s
            start_period: 60s
            start_interval: 10s
            retries: 5
            test: "pg_isready -U $$POSTGRES_USER"
        volumes:
            - netbox-postgres-data:/var/lib/postgresql/data
            - ./docker/postgres/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
    redis:
        image: docker.io/redis:7-alpine
        env_file: env/redis.env
        command:
            - sh
            - -c # this is to evaluate the $REDIS_PASSWORD from the env
            - redis-server --appendonly yes --requirepass $$REDIS_PASSWORD ## $$ because of docker-compose
        volumes:
            - netbox-redis-data:/data
    redis-cache:
        image: docker.io/redis:7-alpine
        env_file: env/redis-cache.env
        command:
            - sh
            - -c # this is to evaluate the $REDIS_PASSWORD from the env
            - redis-server --requirepass $$REDIS_PASSWORD ## $$ because of docker-compose
        volumes:
            - netbox-redis-cache-data:/data
volumes:
    netbox-media-files:
        driver: local
    netbox-postgres-data:
        driver: local
    netbox-redis-cache-data:
        driver: local
    netbox-redis-data:
        driver: local
    netbox-reports-files:
        driver: local
    netbox-scripts-files:
        driver: local
```

Env files are stored in the `env` directory in the [same path to the `docker-compose.yml`](./docker-compose/docker-compose.yml) file.

Each env file contains variable that configure the service inside container:

* `netbox.env`: the Netbox instance configuration (see https://github.com/netbox-community/netbox-docker/wiki/Configuration)
* `postgres.env`: the PostgreSQL configuration (see: https://hub.docker.com/_/postgres)
* `redis-cache.env` and `redis.env`: the configuration for Redis instances password

Finally we start a new Netbox stack with:

```bash
env TAG=latest docker compose up -d
```

### Upgrade Process

## Netbox Docker Plugin

### Installation Process

### Upgrade Process
