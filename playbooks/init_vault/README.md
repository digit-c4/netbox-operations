# Init Development & Testing Vault Instance

Initialize your dev and test Vault instance environment

## How to run

Please start Netbox instance and others services:
```console
env TAG=latest docker-compose up -d
```

In your terminal at the project root, execute:

```console
env VAULT_ADDR=http://localhost:8200 VAULT_AUTH_METHOD=token VAULT_TOKEN=token ansible-playbook playbooks/init_vault/init_vault.yml --extra-vars "@playbooks/init_vault/vars/dev.json"
```

### Environment variables

We use environment variable, as describe on the [Vault Ansible collection
documentation](https://github.com/TerryHowe/ansible-modules-hashivault#environmental-variables).

### Variables

Below the description of configuration directives.

* `netbox`: required, object
  - `name`: required, string: the name of the Netbox instance.
  - `admin`:
    - `username`: required, string: the Netbox administrator username
    - `password`: required, string: the Netbox administrator default password
      ([can be change after initialization](../change_users_password/))
* `vault`: required, object
  - `mount_point`: required, string: the Vault mount point for storing the Netbox user passwords

Example for `dev` environment you can find in the `vars/dev.json` file:

```json
{
    "netbox": {
        "name": "localhost",
        "admin": {
            "username": "admin",
            "password": "thisnetboxisnot4u"
        }
    },
    "vault": {
        "mount_point": "cmdb"
    }
}
```
