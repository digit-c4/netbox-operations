# How to rotate your Netbox password

To enable password rotation, ensure that you have an AWX instance with open connections to both Vault and the Netbox instance you wish to rotate passwords for.
If these requirements are met, you will need to create an AWX Job Template for rotating the password. This will require additional objects such as Project, Password Types, Scheduler, etc. This step is automated in AWX-data repository and you just need to add your AWX instance details.

## Requirements
* AWX **must** have an opened connection to Vault and your Netbox.
* Linux VM (provided by SYS squad will have all required dependencies) with opened connection to your AWX instence.
* Vault placeholder for your Netbox password in cmdb engine. See [vault preparation](#vault-preparation) if missing.

## Vault preparation
Contact CMDB Squad member to create vault placeholder for your Netbox instance. Directly on Teams or create card on the [CMDB board](https://globalntt.leankit.com/board/31512194691730).

## AWX-data preparation: 
### Clone the repository:
``` git clone https://code.europa.eu/digit-c4/awx-data.git ```

### Add your AWX instance details:
1. Create a new branch from ```main```
2. Edit ```ansible/hosts``` file and add your AWX details
```
[service_yourServiceName]
yourAwxHostWithoutHTTPprotocolAndPort
```
*example:*
```
[cmdb_workplace]
vawx-131.lab.snmc.cec.eu.int
```
3. Create ```service_yourServiceName``` folder inside ```awx-data/ansible/group_vars``` and create ```cmdb.yml``` file there - name corespond to the role you are going to execute. See example: ```awx-data/ansible/group_vars/cmdb_workplace/cmdb.yml```
4. Insert below example variables into your file:

*example:*
``` 
awx_host: "https://{{ inventory_hostname }}/"
netbox_host_var: "vnetbox-130.lab.snmc.cec.eu.int" 
ssh_credential_name: "ssh_netbox"
sys_inventory_name: "sys_service_catalogue_inventory"
password_rotation_netbox_base_url: "https://{{ netbox_host_var }}/api"
password_rotation_netbox_name: "workplace/netbox/{{ netbox_host_var }}/all"
```
### where
* ```awx_host``` - Corespond to actual AWX URL with http/https protocol and port if required. Two scenarios possibles: ```http://{{ inventory_hostname }}:8013/``` or ```https://{{ inventory_hostname }}/```
* ```netbox_host_var``` - The FQDN (Fully Qualified Domain Name) of your netbox
* ```ssh_credential_name``` - The name of the credetials used in AWX to connect with Netbox host
* ```sys_inventory_name``` - AWX inventory name where netbox and vault are present. For most cases, this will be ```sys_service_catalogue_inventory```
* ```password_rotation_netbox_base_url``` - similar to ```awx_host```, two scenarios possible: ```http://{{ netbox_host_var }}:8080/api``` or ```"https://{{ netbox_host_var }}/api"```
* ```password_rotation_netbox_name``` - The Vault credentials path for your Netbox instance (see: [vault preparation](#vault-preparation))


## Execute
To execute CMDB role, you will need to set additional environment variables. These are stored in [vault](https://sam-hcpvault.cec.eu.int/ui/vault/secrets/cmdb/kv/netbox_operations%2Fpassword_rotation%2Fall/details?namespace=EC%2FDIGIT_C4_SNET_ADMIN-PROD). Copy them and set them on the VM you are using.

**If you are using a proxy, disable it before exeuting the below command**

```
ansible-playbook $ANSIBLE_LOGLEVEL $ANSIBLE_EXTRA -i ${INVENTORY} ${PLAYBOOK} -l "cmdb_psw_rotation_aws_group" --tags "${TAG_NAME}"
```