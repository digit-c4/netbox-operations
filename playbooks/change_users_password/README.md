# Change Netbox Users Password

Playbook that aims to change Netbox users password. Changed passwords are stored
on a configured Vault instance.

## How to run

### Development

In development environment, please start Netbox instance and others services:

```console
env TAG=latest docker-compose up -d
```

In your terminal at the project root, execute:

```console
env VAULT_ADDR=http://localhost:8200 VAULT_AUTH_METHOD=token VAULT_TOKEN=token ansible-playbook playbooks/change_users_password/change_users_password.yml --extra-vars "@playbooks/change_users_password/vars/dev.json"
```

### Environment variables

We use environment variable, as describe on the [Vault Ansible collection
documentation](https://github.com/TerryHowe/ansible-modules-hashivault#environmental-variables).


### Variables

Below the description of configuration directives.

* `netbox`: required, object
  - `base_url`: required, string: the Netbox API base url
  - `name`: required, string: the name of the Netbox instance.
  - `admin`:
    - `username`: required, string: the Netbox administrator username
* `usernames`: required, Array<string>: the list of Netbox username to change
  the password
* `vault`: required, object
  - `mount_point`: required, string: the Vault mount point for storing the Netbox user passwords

Example for `dev` environment you can find in the `vars/dev.json` file:

```json
{
    "netbox": {
        "base_url": "http://localhost:8080/api",
        "name": "localhost",
        "admin": {
            "username": "admin"
        }
    },
    "usernames": ["admin", "snet"],
    "vault": {
        "mount_point": "cmdb"
    }
}

```

### Execute tests

With pytest:

```console
env ADMIN_PASSWORD="..." pytest -s tests/change_users_password/
```

By default after vault initialization `ADMIN_PASSWORD` is equal to
`thisnetboxisnot4u`. You can change the environment variable `ADMIN_PASSWORD`
with the correct current value you can find in vault:

```console
VAULT_ADDR='http://127.0.0.1:8200' VAULT_TOKEN="token" vault kv get -mount="cmdb" "localhost/admin"`
```

The output of the previous command will look like:

```console
====== Secret Path ======
cmdb/data/localhost/admin

======= Metadata =======
Key                Value
---                -----
created_time       2023-12-19T15:02:46.440788136Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            11

====== Data ======
Key         Value
---         -----
password    somerandompassword
```

Please report the `password` key on `ADMIN_PASSWORD` environment variable.
