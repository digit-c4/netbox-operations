---

- name: "Trigger Docker container recreation : {{ task_suffix }}"
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}/"
    method: PATCH
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      operation: recreate
    body_format: json
    status_code: 200
    validate_certs: false

- name: Check that you can connect (GET) to a page and it returns a status 200
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}/"
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
  # retry in case is upgrading same container we are connecting to.
  retries: 10
  delay: 10

- name: "Fetch NEW Docker ContainerID from Netbox : {{ task_suffix }}"
  ansible.builtin.set_fact:
    docker_container_new_tmp: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'containers',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s host_id=%d' | format(
              docker_container_name,
              docker_host_id | int
            )
          )
        ] | flatten
          | first
          | community.general.json_query('value')
      }}

- name: Print return information from the previous task
  ansible.builtin.debug:
    var: "{{ item_debug }}"
  loop:
    - docker_container.ContainerID
    - docker_container_new_tmp.ContainerID
  loop_control:
    loop_var: item_debug

- name: Print return information from the previous task
  when: docker_container_new_tmp.ContainerID == docker_container.ContainerID
  ansible.builtin.debug:
    msg: "same ContainerID, we should refresh host."

- name: "Trigger Host refresh : {{ task_suffix }}"
  when: docker_container_new_tmp.ContainerID == docker_container.ContainerID
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/hosts/{{ docker_host_id }}/"
    method: PATCH
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      operation: refresh
    body_format: json
    status_code: 200
    validate_certs: false

- name: Giving some room..
  when: docker_container_new_tmp.ContainerID == docker_container.ContainerID
  ansible.builtin.pause:
    seconds: 30

# this might fail if same postgres we are connecting to. But still could have been a success..
- name: "Wait for Docker Container to be ready : {{ task_suffix }}"
  when: docker_container_new_tmp.ContainerID != docker_container.ContainerID
  ec.rps_nginx.netbox_poll_journal:
    netbox_api: "{{ netbox_api }}"
    netbox_token: "{{ netbox_token }}"
    validate_certs: false
    object_type: "netbox_docker_plugin.container"
    object_id: "{{ docker_container.id }}"
    comments_filter: "operation recreate"
