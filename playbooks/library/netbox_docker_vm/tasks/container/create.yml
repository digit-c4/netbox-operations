---

- name: "Fetch Docker Container from Netbox : {{ task_suffix }}"
  ansible.builtin.set_fact:
    docker_container_response: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'containers',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s host_id=%d' | format(
              docker_container_name,
              docker_host_id | int
            )
          )
        ] | flatten
      }}

- name: "Create Docker container : {{ task_suffix }}"
  when: docker_container_response | length == 0
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/"
    method: POST
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      operation: create
      name: "{{ docker_container_name }}"
      image: "{{ docker_img_id }}"
      host: "{{ docker_host_id }}"
      env: "{{ docker_container_env }}"
      ports: "{{ docker_container_ports }}"
      binds: "{{ docker_container_binds }}"
      mounts: "{{ docker_container_mounts }}"
      labels: "{{ docker_container_labels }}"
      hostname: "{{ docker_container_hostname }}"
      network_settings: "{{ docker_container_network_settings }}"
      restart_policy: "{{ docker_container_restart }}"
    body_format: json
    status_code: 201
    validate_certs: false

- name: "Fetch Docker container information : {{ task_suffix }}"
  when: docker_container_response | length == 0
  ansible.builtin.set_fact:
    docker_container: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'containers',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s host_id=%d' | format(
              docker_container_name,
              docker_host_id | int
            )
          )
        ] | flatten
          | first
          | community.general.json_query('value')
      }}

- name: "Wait for Docker Container to be created : {{ task_suffix }}"
  when: docker_container_response | length == 0
  ec.rps_nginx.netbox_poll_journal:
    netbox_api: "{{ netbox_api }}"
    netbox_token: "{{ netbox_token }}"
    validate_certs: false
    object_type: "netbox_docker_plugin.container"
    object_id: "{{ docker_container.id }}"
    comments_filter: "container created"

- name: "Extract Docker container information : {{ task_suffix }}"
  when: docker_container_response | length > 0
  ansible.builtin.set_fact:
    docker_container: |
      {{
        docker_container_response
          | first
          | community.general.json_query('value')
      }}

- name: "Update Docker container : {{ task_suffix }}"
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}/"
    method: PATCH
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      name: "{{ docker_container_name }}"
      image: "{{ docker_img_id }}"
      host: "{{ docker_host_id }}"
      env: "{{ docker_container_env }}"
      ports: "{{ docker_container_ports }}"
      binds: "{{ docker_container_binds }}"
      mounts: "{{ docker_container_mounts }}"
      labels: "{{ docker_container_labels }}"
      hostname: "{{ docker_container_hostname }}"
      network_settings: "{{ docker_container_network_settings }}"
      restart_policy: "{{ docker_container_restart }}"
    body_format: json
    status_code: 200
    validate_certs: false

- name: "Trigger Docker container recreation : {{ task_suffix }}"
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}/"
    method: PATCH
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      operation: recreate
    body_format: json
    status_code: 200
    validate_certs: false

- name: "Wait for Docker Container to be ready : {{ task_suffix }}"
  ec.rps_nginx.netbox_poll_journal:
    netbox_api: "{{ netbox_api }}"
    netbox_token: "{{ netbox_token }}"
    validate_certs: false
    object_type: "netbox_docker_plugin.container"
    object_id: "{{ docker_container.id }}"
    comments_filter: "operation recreate"

