---

- name: "Fetch Docker Container from Netbox : {{ task_suffix }}"
  ansible.builtin.set_fact:
    docker_container_response: |
      {{
        [
          lookup(
            'netbox.netbox.nb_lookup',
            'containers',
            plugin='docker',
            api_endpoint=netbox_api,
            token=netbox_token,
            validate_certs=False,
            api_filter='name=%s host_id=%d' | format(
              docker_container_name,
              docker_host_id | int
            )
          )
        ] | flatten
      }}

- name: "Extract Docker container information : {{ task_suffix }}"
  when: docker_container_response | length > 0
  ansible.builtin.set_fact:
    docker_container: |
      {{
        docker_container_response
          | first
          | community.general.json_query('value')
      }}

- name: "Stop Docker container : {{ task_suffix }}"
  when:
    - docker_container_response | length > 0
    - docker_container.state == "running"
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}/"
    method: PATCH
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body:
      operation: stop
    body_format: json
    status_code: 200
    validate_certs: false

- name: "Wait for Docker Container to stop : {{ task_suffix }}"
  when:
    - docker_container_response | length > 0
    - docker_container.state == "running"
  ec.rps_nginx.netbox_poll_journal:
    netbox_api: "{{ netbox_api }}"
    netbox_token: "{{ netbox_token }}"
    validate_certs: false
    object_type: "netbox_docker_plugin.container"
    object_id: "{{ docker_container.id }}"
    comments_filter: "operation stop"

- name: "Remove Docker container : {{ task_suffix }}"
  when: docker_container_response | length > 0
  ansible.builtin.uri:
    url: "{{ netbox_api }}/api/plugins/docker/containers/{{ docker_container.id }}"
    method: DELETE
    headers:
      Authorization: "Token {{ netbox_token }}"
      Content-Type: "application/json"
      Origin: "{{ netbox_api }}"
    body_format: json
    status_code: 204
    validate_certs: false
