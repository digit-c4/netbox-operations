
**Description**

This Ansible library helps to Create, Delete or Upgrade containers on Netbox docker plugin.

It will need a Netbox with [Docker-plugin](https://github.com/SaaShup/netbox-docker-plugin)  
and a VM host connected to pluging through [Docker-agent](https://github.com/SaaShup/netbox-docker-agent).

**Requirements**

It needs python3.8 or higher.

e.g.:
```
python3 -m venv venv
source ./venv/bin/activate
ansible-galaxy collection install -r requirements.yml
pip3 install ansible pynetbox jmespath passlib pytz bcrypt
```

Optional:

* `bcrypt` package.

**How to use on your playbook**

```
- name: "Using library to check for container, volumes and network name"
  ansible.builtin.include_tasks: library/netbox_docker_vm/provisions/upgrade_checks.yml
  loop:
      ######################
     # postgres checks    #
    ######################
    - task_suffix: "postgres"
      docker_host_name: "<hostname of an already set-up host on docker plugin>"
      docker_network_name: "my_new_created_network"
      docker_cont_name: "my-great-container-postgres-1"
      docker_cont_mounts:
        - { volume: { name: "my-great-volume-postgres-data", driver: "local" }, source: "</absolute/destination/path>", read_only: false}
        - { volume: { name: "my-great-volume2-postgres-data2", driver: "local" }, source: "/var/lib/postgresql/data", read_only: false}

- name: "Using library to remove a container, will NOT remove volumes."
  ansible.builtin.include_tasks: library/netbox_docker_vm/provisions/remove_container.yml
  loop:
      ######################
     # postgres remove    #
    ######################
    - task_suffix: "postgres"
      docker_host_name: "<hostname of an already set-up host on docker plugin>"
      docker_cont_name: "my-great-container-postgres-1"

- name: "Using library to create a container"
  ansible.builtin.include_tasks: library/netbox_docker_vm/provisions/create_container.yml
  loop:
      ######################
     # postgres set-up    #
    ######################
    - task_suffix: "postgres"
      docker_host_name: "<hostname of an already set-up host on docker plugin>"
      docker_network_name: "my_new_created_network"
      docker_reg_name: "dockerhub"
      docker_reg_serveraddress: "https://registry.hub.docker.com/v2/"
      docker_reg_username: ""
      docker_reg_password: ""
      docker_img_name: "postgres"
      docker_img_version: "15"
      docker_cont_name: "my-great-container-postgres-1"
      docker_cont_env:
        - { var_name: POSTGRES_DB, value: netbox }
        - { var_name: POSTGRES_PASSWORD, value: a_great_password }
        - { var_name: POSTGRES_USER, value: netbox }
      docker_cont_ports:
        - public_port: 5432
          private_port: 5432
          protocol: "tcp"
        - public_port: 443
          private_port: 443
          protocol: "tcp"
      docker_cont_binds: []
      #docker_cont_binds:
      #  - container_path: "/var/run/docker.sock"
      #    host_path: "/var/run/docker.sock"
      docker_cont_mounts:
        - { volume: { name: "my-great-volume-postgres-data", driver: "local" }, source: "</absolute/destination/path>", read_only: false}
        - { volume: { name: "my-great-volume2-postgres-data2", driver: "local" }, source: "/var/lib/postgresql/data", read_only: false}
      docker_cont_restart: "no"

- name: "Using library to remove older docker dangling images"
  ansible.builtin.include_tasks: library/netbox_docker_vm/provisions/remove_image.yml
  when: item.docker_img_version | length > 0
  loop:
      #######################
     # remove postgres img #
    #######################
    - task_suffix: "rm postgres"
      docker_host_name: "<hostname of an already set-up host on docker plugin>"
      docker_network_name: "my_new_created_network"
      docker_reg_name: "dockerhub"
      docker_reg_serveraddress: "https://registry.hub.docker.com/v2/"
      docker_reg_username: ""
      docker_reg_password: ""
      docker_img_name: "postgres"
      docker_img_version: "14"

```

**Roles using this library**

- [../../roles/netbox_galaxy/](../../roles/netbox_galaxy/)
- [../../roles/netbox_api/](../../roles/netbox_api/)
