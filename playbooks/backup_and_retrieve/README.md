# Backup and Restore

Playbook that aims to backup and restore Netbox database dump (or another file).
Ansible will be used in order to backup files.

## How to run

### Development

In development environment, please start Netbox instance and others services:

```console
env TAG=latest docker-compose up -d
```

In your terminal at the project root, execute:

```console
 ansible-playbook playbooks/backup_and_retrieve/backup.yml -K --extra-vars "@playbooks/backup_and_retrieve/vars/dev.json"
```

### Variables

Below the description of configuration directives.

* `netbox_host`: required, string: the host of Netbox instance connected with DB to restore.
* `netbox_host_backup_src_path`: required, string: the source path to the file to be backup-ed, **without / at the end**
* `archive_server`: required, string: the full address of the archive server. **Needs to be listed in ansible inventory**.
* `archive_server_path`: required, string: the destination path to store file, **without / at the end**

Example for `dev` environment you can find in the `vars/dev.json` file:

```json
{
  "netbox_host": "localhost",
  "netbox_host_backup_src_path": "/tmp",
  "archive_server": "localhost",
  "archive_server_path": "/tmp/backup"
}

```

### Execute tests

With pytest:

```console
pytest -s tests/backup_and_retrieve/
```
