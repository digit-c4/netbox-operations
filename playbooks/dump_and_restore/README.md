# Dump and restore Netbox database

Collection of ansible playbook that dump and restore Netbox database.

## Requirements

Host that execute playbook must have access to PostgreSQL command tools (psql,
pg_dump and pg_restore) and have access to PostgreSQL instance that we want to
dump or restore.

## How to run

### Dump the database

In your terminal at the project root, execute:

```console
ansible-playbook playbooks/dump_and_restore/dump.yml --extra-vars "@playbooks/dump_and_restore/vars/dev.json"
```

This playbook will produce a SQL file in the directory set for this purpose (see
[Variables section](#variables)). The name of the file is the name of the
database we dump.

### Restore the database

```console
ansible-playbook playbooks/dump_and_restore/restore.yml --extra-vars "@playbooks/dump_and_restore/vars/dev.json"
```

This playbook will restore the database with the SQL file present in the
directory set for this purpose (see [Variables section](#variables)). The name
of the file is the name of the database we want to restore.

### Variables

Below the description of configuration directives.

* `netbox_host`: required, string: the host of Netbox instance connected with DB to restore.
* `postgres`: required, object
  - `container`: required, string: the name of PostgreSQL container where DB instance is deployed
  - `host`: required, string: the host of PostgreSQL instance.
  - `port`: required, string: the exposed port number of PostgreSQL container.
  - `user`: required, string: the username playbook should use to establish its PostgreSQL session.
  - `database`: required, string: the name of the database to dump or restore.
  - `password`: required, string: The password playbook should use to establish its PostgreSQL session.
* `dump`: required, object
  - `base_path`: required, string: the directory where to save dump file or find restore file.
  - `file_name`: required, string: dump file name to restore.

Example for `dev` environment you can find in the `vars/dev.json` file:

```json
{
  "netbox_host": "localhost",
  "postgres": {
    "container": "netbox-operations-postgres-1",
    "host": "localhost",
    "port": "5432",
    "user": "netbox",
    "database": "netbox",
    "password": "J5brHrAXFLQSif0K"
  },
  "dump": {
    "base_path": "/tmp",
    "file_name": "postgresql_dump.sql"
  }
}

```
## How to fetch backup file from storage server.

Server with files is `vlog-jmo.snmc.cec.eu.int`, path `/mnt/bk/netbox/`.  
you can connect through ssh or just run the following commands to fetch the file.
```
 # Syntax.
 # scp <SNET_USER>@vlog-jmo.snmc.cec.eu.int:/mnt/bk/netbox/<SERVER_TO_RESTORE>_<YYYY><MM><DD>T*.sql.tar.* /tmp/.
scp bob@vlog-jmo.snmc.cec.eu.int:/mnt/bk/netbox/vnetbox-dev.dev.snmc.cec.eu.int_20240513T*.sql.tar.* /tmp/.
```

Once is on your local machine you can extract.
```
 # tar command example to extract file into /tmp folder
tar -C / -xzf /tmp/vnetbox-dev.dev.snmc.cec.eu.int_20240513T*.sql.tar.gz
```

Now you can just run ansible playbook [restore.yml](restore.yml), to restore a target netbox with downloaded file.
```
 # Use your custom varibles inside `playbooks/dump_and_restore/vars/dev.json` file.
 # simple command example.
ansible-playbook -i <your_inventory_file> restore.yml --extra-vars "@vars/dev.json"
 # more complex command example.
ansible-playbook -i mylab.com, restore.yml --extra-vars "@vars/dev.json"  -e '{"dump": {"file_name": "vnetbox-dev.dev.snmc.cec.eu.int_20240513T165846.sql", "base_path": "/tmp" }}' -e 'ansible_user=debian netbox_host=mylab.com'
```
