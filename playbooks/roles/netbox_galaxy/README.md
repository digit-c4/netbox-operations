## Create or Upgrade a Netbox using [Docker-agent](https://github.com/SaaShup/netbox-docker-agent).

**Requirements**

This role depends on the following library.  
Read library's requirements, ["../../library/netbox_docker_vm/"](../../library/netbox_docker_vm/).

To run "Netbox-galaxy" role you will need a Netbox with [Docker-plugin](https://github.com/SaaShup/netbox-docker-plugin),  
an already connected VM host to that plugin with [Docker-agent](https://github.com/SaaShup/netbox-docker-agent), and Debian 12 on target.

You can setup a local environment following ["docs/deploy_local.md"](docs/deploy_local.md).

To set your environment variables:

```
 # copy example environments file to a file name starting with ".env"
cp env.example .env_mylab

 # Time to edit variables on your new created file.
nano .env_mylab
```

**How to run Netbox Galaxy role**

In the folder /playbooks/roles/netbox_galaxy execute:
```
 # export target variables && deploy.
source .env_mylab
ansible-playbook -i <server_to_deploy_playbook>, -u debian ../../netbox_galaxy.yml -k

```
* If you want to specify the password because you might login in the server with a password, you can add -k to the command. 
* If you want to more debug information on what the playbook is doing add -vvvv to the command.

**Troubleshooting**

["docs/troubleshooting.md"](docs/troubleshooting.md).

KNOW ISSUE:

- Docker images upgrades with same tag name, example 'latest', will use already downloaded image, and new pull image won't be requested.
