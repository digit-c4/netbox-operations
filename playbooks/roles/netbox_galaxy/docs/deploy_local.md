## Deploy locally:


**1. Run netbox with docker plugin:**
* Start the local netbox instance by:
On the netbox-operation repository execute
```
env TAG=latest docker-compose up -d
```
<br>

**2. Run Docker agent:**
* Grant RW Access to the docher unix socker:
```
sudo chmod 666 /var/run/docker.sock
```
* Make sure you are not using older tag versions. Tested with 0.30.0 tag.
```
git pull https://github.com/SaaShup/netbox-docker-agent.git
git checkout -b some 0.30.0
```
* Add variable to docker_agent Dockerfile:
```
ENV NETBOX_URL=http://<IP of netbox>:<port of netbox>
ENV NODE_EXTRA_CA_CERTS=/etc/ssl/cert.pem
```
* Build:
```
docker build -t saashup/netbox-docker-agent .
```
* Run:
```
docker run -d -p 1880:1880 -v /var/run/docker.sock:/var/run/docker.sock:rw -v netbox-docker-agent:/data --name netbox-docker-agent saashup/netbox-docker-agent
```
* Clean up(if necessary): 
```
docker stop netbox-docker-agent
docker rm netbox-docker-agent
docker image rm saashup/netbox-docker-agent
docker volume rm netbox-docker-agent
```
For more information (login information & more) follow the link: https://github.com/SaaShup/netbox-docker-agent

<br>

**3. Connect docker agent to Netbox:**
* Add a new host:
Go to the docker plugin Hosts form and add a new host with the following information:
Name: You can choose the name!
Endpoint:
```
 http://<docker agent user>:<docker agent password>@<IP where the docker agent is deployed>:1880
```
* Check if the connection was successfull:
In the docker agent home page you should see a green "ok"

<br>

**4. Install Galaxy:**

* Install requirements:
In the folder /playbooks/library/netbox_docker_vm execute:
```
ansible-galaxy collection install -r requirements.yml
pip3 install pynetbox jmespath passlib
```
For more information follow the link: ["../../../library/netbox_docker_vm/"](../../../library/netbox_docker_vm/).

**Run the ansible playbook:**

["../README.md"](../README.md).


**Possible errors:**

["troubleshooting.md"](troubleshooting.md).
