## Troubleshooting

This role will not work with "Netbox Plugins Distribution v3.6.9-r1" or lower , and ansible variable "gxy_netbox_docker_img_version=" can be set, to use newer tag.

---

**Host errors:**

- Target "Netbox docker plugin" should already have a host connected properly to a "Netbox docker agent".
- When creating Host manually from "Netbox docker plugin", URL should be and IP, and endpoint as well. Not alias DNS names.
- Extra notes: If setting variable "NETBOX_URL", on your "Netbox docker agent" when created, it will always connect to that variable URL.

---

**Get stuck waiting for image to download:**

Network, Registries and Images should have been created through playbook, or "Netbox docker agent", avoid manual way.  
If you are installing the netbox galaxy in the same server where you executed netbox there might be "stalling" issues on the tasks related to  
"Wait for Docker Container to be ready:". If this is the case stop the execution (ctrl-C) and retry.

You might need to run a couple of times until succeed.

You might need to reconnect docker host endpoint to "docker agent" again. Click Edit to see password, delete, and introduce same name and values as it was.

---

**Backup-agent does not produce backup files:**

Go to netbox backup-agent container >> ENV variables edit `NETBOX_BASE_URL` >>  
try with IP, or test that is the correct URL, maybe something like `http://10.67.5.215:8080`, save >>  
back to container `netbox-gxy-backup-agent-0` and `recreate` container.


