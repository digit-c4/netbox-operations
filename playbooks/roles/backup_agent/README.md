## Backup-agent role

This role will connect to a remote "Netbox Backup Agent", create backup, fetch to ansible host and push to "vlog" server right after.

**Requirements**

To declare Environment variables:

```
export \
BCKP_AGT_API=http://<your_target_backup_agent_domain_or_IP>:1881 \
BCKP_AGT_USERNAME=<backup_agent_username> \
BCKP_AGT_PASSWORD=<backup_agent_password> \
BCKP_AGT_DEST_PATH=/tmp/
BCKP_ARCHIVE_SERVER=<optional_vlog_server>
BCKP_USER_SERVER=<optional_user_for_vlog_server>
BCKP_PASS_SERVER=<some_snet_scp_password_for_vlog_archive_server>
```

**How to run**

e.g.:

```
ansible-playbook -i <some_server_will_use_to_fecth_and_push_to_vlog>, -u <your_remote_user> ../../backup_agent.yml -k
```

Host should be able to reach Netbox-backup-agent network for this playbook to work.
