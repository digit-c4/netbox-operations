# Netbox docker agent role.

Role to install Netbox-docker-agent.

This role has been adapted from "sys-service-catalogue" repo.
[https://code.europa.eu/digit-c4/sys-service-catalogue/-/tree/main/ansible/playbooks/roles/netbox_docker_agent](https://code.europa.eu/digit-c4/sys-service-catalogue/-/tree/main/ansible/playbooks/roles/netbox_docker_agent?ref_type=heads)

**How to run, example command**

Example use User(`debian`), Pass(`saashup`).

```
ansible-playbook -i mylab, -u debian ../../netbox_docker_agent_install.yml -e '\
NETBOX_URL=http://<some_netbox>:8080 \
API_USERNAME="debian" \
API_PASSWORD="$2a$08$s.NFdSn4Gm4d7gHErya//e6O8RO1/3f7TZ7zflXJ9jfFV0cI6jGwK" \
ADMIN_USERNAME="debian" \
ADMIN_PASSWORD="$2a$08$s.NFdSn4Gm4d7gHErya//e6O8RO1/3f7TZ7zflXJ9jfFV0cI6jGwK" \
ansible_user=debian'
```

**To know**

It will restart `Docker socket`, that might put down any other container you had up and running.
