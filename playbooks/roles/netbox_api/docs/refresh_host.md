# TAG, refresh_host


## Refresh docker plugin target host, Flowchart.

It will delete host, and create again with same values.

WARNING: Do not stop playbook while running!!!.

```mermaid
flowchart TD
 subgraph subGraph2["VM-1."]
        O1("fa:fa-box Traefik")
        P1("fa:fa-box Service2")
  end
 subgraph subGraph3["VM-2."]
        O2("fa:fa-box Service4")
        P2("fa:fa-box Sub-Netbox")
  end
 subgraph subGraph4["VM-3."]
        O3("fa:fa-box Sub-Netbox")
        P3("fa:fa-box Service3")
  end
 subgraph subGraph1["Bubble-Netbox."]
        M("Netbox docker-plugin")
        subGraph2
        subGraph3
        subGraph4
  end
    AA("fa:fa-users Bubble Developer") --> C(["fa:fa-sun Ansible-Role"])
    C ==> M
    M -- delete and create host with same values --> subGraph2
    M ~~~ O2 & O3

    style C fill:#E1BEE7
    linkStyle 1 stroke:#AA00FF,fill:none
```

## Sequence Diagram

```mermaid
sequenceDiagram
  title Install Traefik container
  %% Install container
  Actor p0 as Release Manager
  participant p1 as Ansible-host
  participant serv as Netbox-docker-plugin
  participant v1 as Target VM


  p0 ->> p1 : run playbook
  p1 ->> serv : Refresh host
  alt If Host found
    serv ->> v1 : Delete host
    v1 ->> serv : Ok
    serv ->> serv : Sleep 30 secs.
    serv ->> v1 : Create host with same values.
    v1 ->> serv : Ok
  end
  serv ->> p1 : Ok
  p1 ->> p0   : Ok
```
