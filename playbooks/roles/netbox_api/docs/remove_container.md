# TAG, remove_xxxxx


## Remove container, Flowchart.

In this flowchart, it will check for `sub-netbox` inside a bubble and target host,
read containers with that use specic image name, stop them and remove them, without remove volumes.


```mermaid
flowchart TD
 subgraph subGraph2["VM-1."]
        N1("fa:fa-database container_using_target_image")
        O1("fa:fa-box Sub-Netbox1")
        P1("fa:fa-box container2_using_target_image")
  end
 subgraph subGraph3["VM-2."]
        O2("fa:fa-box Sub-Netbox2")
        P2("fa:fa-box Postgres2")
  end
 subgraph subGraph4["VM-3."]
        N3("fa:fa-database Backup-agent")
        O3("fa:fa-box Sub-Netbox3")
        P3("fa:fa-box Postgres3")
  end
 subgraph subGraph1["Bubble-Netbox."]
        M("Netbox docker-plugin")
        subGraph2
        subGraph3
        subGraph4
  end
    AA("fa:fa-users Bubble Developer") --> C(["fa:fa-sun Ansible-Role"])
    C ==> M
    M -- remove_container --> N1
    M -- remove_container --> P1
    M ~~~ N3

    style C fill:#E1BEE7
    linkStyle 1 stroke:#AA00FF,fill:none
```

## Sequence Diagram

By reading containers info before removing.

```mermaid
sequenceDiagram
  title Remove container
  %% Remove container
  Actor p0 as Release Manager
  participant p1 as Ansible-host
  participant serv as Netbox-docker-plugin
  participant v1 as Target host, VM.

  p0 ->> p1 : run playbook
  p1 ->> serv : Get containers info
  serv ->> p1 : Ok
  p1 ->> serv : Remove container
  loop For each Container
    alt If Container using target Image
      serv ->> v1 : stop container
      v1 ->> serv : Ok
      serv ->> v1 : remove container
      v1 ->> serv : Ok
    end
  end
  serv ->> p1 : Ok
  p1 ->> p0   : Ok
```

