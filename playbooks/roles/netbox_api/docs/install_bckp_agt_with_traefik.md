# TAG, install_bckp_agt_with_traefik


## Install container(backup-agent), Flowchart.

In this flowchart, it will check for `sub-netbox` inside a bubble and target host that do not have `backup-agent` within it, read netbox configuration,  
and install `backup-agent` alongside.  

If postgres container has no dump volume, it will create that volume beforehand.  
If there is not traefik on VM, it will install it.  
If several Netbox on one VM, it will install backup-agent in all of the ones that do not have it.


```mermaid
flowchart TD
 subgraph subGraph2["VM-1."]
        N1("fa:fa-database Backup-agent")
        O1("fa:fa-box Sub-Netbox1")
        P1("fa:fa-box Postgres1")
        Q1("fa:fa-box Traefik")
  end
 subgraph subGraph3["VM-2."]
        O2("fa:fa-box Sub-Netbox2")
        P2("fa:fa-box Postgres2")
  end
 subgraph subGraph4["VM-3."]
        N3("fa:fa-database Backup-agent")
        O3("fa:fa-box Sub-Netbox3")
        P3("fa:fa-box Postgres3")
  end
 subgraph subGraph1["Bubble-Netbox."]
        M("Netbox docker-plugin")
        subGraph2
        subGraph3
        subGraph4
  end
    AA("fa:fa-users Bubble Developer") --> C(["fa:fa-sun Ansible-Role"])
    C ==> M
    M -- create_container --> N1
    M -- add_dump_volume --> P1
    M -- create_container --> Q1
    M ~~~ N3

    style C fill:#E1BEE7
    linkStyle 1 stroke:#AA00FF,fill:none
```

## Sequence Diagram

By reading containers info before install, we will be able to configure backup-agent during installation process.

```mermaid
sequenceDiagram
  title Install Backup-agent container
  %% Install container
  Actor p0 as Release Manager
  participant p1 as Ansible-host
  participant serv as Netbox-docker-plugin
  participant v1 as Target host, VM.


  p0 ->> p1 : run playbook
  p1 ->> serv : Get containers info
  serv ->> p1 : Ok
  p1 ->> serv : Install backup-agent
  loop For each Netbox
    alt If Postgres without dump volume
      serv ->> v1 : add dump volume to postgres container
      v1 ->> serv : Ok
    end
    alt If Netbox without backup-agent
      serv ->> v1 : install backup-agent
      v1 ->> serv : Ok
    end
  end
  alt If VM without traefik
    serv ->> v1 : install traefik
    v1 ->> serv : Ok
  end
  serv ->> p1 : Ok
  p1 ->> p0   : Ok
```

