## Create or Upgrade containers on a Netbox using [Docker-agent](https://github.com/SaaShup/netbox-docker-agent).

**About**

This role install containers using netbox API.

**Requirements**

This role depends on the following library.  
Read library's requirements, ["../../library/netbox_docker_vm/"](../../library/netbox_docker_vm/).

**How to run**

* If host needs a password, you can add `-k` to the command.
* For extra verbosity, add `-vvvv` to the command.
* If host is local, `--connection=local`.
* If some 'locale' errors, `env LC_ALL=C.UTF-8 ...`

e.g.:
```
env \
NETBOX_API=http://<some_netbox>:8080 \
NETBOX_TOKEN="sometokentousexxx" \
NETBOX_DOCKER_HOST_NAME="some8" \
ansible-playbook -i <username>@<change_for_your_host>vworker-dev.dev.snmc.cec.eu.int, -u <username> ../../netbox_api.yml --tags "install_traefik"
```

**Posible tags**

- [install_traefik](docs/install_traefik.md)
- [remove_traefik](docs/remove_container.md)
- [install_bckp_agt](docs/install_bckp_agt.md)
- [install_bckp_agt_with_traefik](docs/install_bckp_agt_with_traefik.md)
- [refresh_host](docs/refresh_host.md)
- ..
