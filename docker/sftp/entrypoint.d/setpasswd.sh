#!/usr/bin/env bash

set -e

echo 'netbox-bck:$6$kzkeVjbWzixxaWUy$5c7yurn/bcLYBSvbnjQutdDnIVmuHuEEdb8YYNuo1X0Tv0urGt0R/mJWwvva31JVplJ4tDjaVF4oK59EPdm/o.' | chpasswd --encrypted
mkdir -p /home/netbox-bck/test
chmod a+rwx /home/netbox-bck/test
