"""Test case for Change Netbox users password"""

import os
from ansible_runner import run_command
import requests

ADMIN_PASSWORD = os.getenv("ADMIN_PASSWORD", default="thisnetboxisnot4u")


def test_that_admin_password_is_changed():
    """Assert that admin password is changed"""

    env = {
        "VAULT_ADDR": "http://localhost:8200",
        "VAULT_AUTH_METHOD": "token",
        "VAULT_TOKEN": "token",
    }

    response = requests.post(
        url="http://localhost:8080/api/users/tokens/provision/",
        json={
            "username": "admin",
            "password": ADMIN_PASSWORD,
        },
        timeout=5,
    )

    assert response.status_code == 201

    out = run_command(
        executable_cmd="ansible-playbook",
        cmdline_args=[
            "playbooks/change_users_password/change_users_password.yml",
            "--extra-vars",
            "@playbooks/change_users_password/vars/dev.json",
        ],
        envvars=env,
    )

    assert out[1] == ""

    response = requests.post(
        url="http://localhost:8080/api/users/tokens/provision/",
        json={
            "username": "admin",
            "password": ADMIN_PASSWORD,
        },
        timeout=5,
    )

    assert response.status_code == 403
