"""Test case for Change Netbox users password"""

import os
from ansible_runner import run_command

ADMIN_PASSWORD = os.getenv("ADMIN_PASSWORD", default="thisnetboxisnot4u")


def test_that_admin_password_is_changed():
    """Assert that admin password is changed"""

    env = {
        "VAULT_ADDR": "http://localhost:8200",
        "VAULT_AUTH_METHOD": "token",
        "VAULT_TOKEN": "token",
    }

    out = run_command(
        executable_cmd="ansible-playbook",
        cmdline_args=[
            "playbooks/backup_and_retrieve/backup.yml",
            "-e",
            "backup_src=" + os.getcwd() + "/tests/backup_and_retrieve/assets/truc.txt",
            "-e",
            "backup_dest=/test/truc.txt",
            "--extra-vars",
            "@playbooks/backup_and_retrieve/vars/dev.json",
        ],
        envvars=env,
    )

    assert out[1] == ""
